# C++ Fundamentals KS

A C++ Fundamentals Knowledge Sharing experience.

C++ is one of the most popular programming languages in the world. It is used for everything, from systems-level programming to mobile app development, and is a solid foundation for every programmer's skill set. Let's discuss essentials from legacy c++98/03 to modern c++11, and best practices.

---
## DISCLAIMER
> This project is under constructions.
---

## Download

Download the zip archive and extract the files or clone the project:

```bash
  git clone --recurse-submodules hgit@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git
```

### Build Requirements

* [g++](https://gcc.gnu.org/) (4.8 or higher)
* [cmake](https://cmake.org/) (3.20.0 or higher)
* * [make](https://www.gnu.org/software/make/) (4.2.1 or higher)

### Build Process

With `cmake`

```bash
# From the root directory

# Generate the build system
# run cmake . -B <build_directory> -D CMAKE_BUILD_TYPE=[Debug|Release]
# Example
cmake . -B build/Debug/ -D CMAKE_BUILD_TYPE=Debug

# Build the project
# run cmake --build <build_directory> --target <TARGET>
# Example
cmake --build/Debug --target TicTacToe
```

Alternatively, you can try to compile each file individually

```bash
# From the <project> directory
# Example: Tic Tac Toe
cd src/10_TicTacToe/Main/

# Compile main.cpp, output to main, and include the Standard Library
g++ -std=c++11 main.cpp -o main -I../../../include/CppStandardLibrary 
```

Or you can setup your Integrated Development Environment (IDE) with cmake but, instructions for that, are outside the scope of this README (which can be updated with images if you want).

## Main Projects

If using `cmake`, all projects include a pre_compiled_headers library of the C++ Standard Library and a project_global_settings Interface Target to be linked to individual projects. 

### Essential Training

* Basic Syntax
* Data Types
* Operators
* Functions
* Classes and Objects
* Templates
* Standard Library
* Standard Template Library

### Sample Game
* Tic Tac Toe

||||
|:-:|:-:|:-:|
|X|O|-|
|X|X|O|
|O|O|X|

## Examples Projects

The examples folder contains two example libraries which can be used to explore C++ code with efficiency and flexibility in mind.

### Effective C++: 55 Specific Ways to Improve Your Programs and Designs

* Effective C++ is a book from Scott Meyes that describes...  (read the title).
* You can buy the book at [Amazon](https://www.amazon.com/Effective-Specific-Improve-Programs-Designs/dp/0321334876).
* The example code is from the second edition which can be found [here](https://github.com/BartVandewoestyne/Effective-Cpp.git).

### Modern C++ Design: Generic Programming and Design Patterns Applied

* Modern C++ Design is a book from Andrei Alexandrescu that describes "generic patterns" as a powerful way of creating extensible designs in C++.
* You can buy the book at [Amazon](https://www.amazon.com/Modern-Design-Generic-Programming-Patterns/dp/0201704315).
* You can find the source code [here](http://loki-lib.sourceforge.net/).

## External Projects

At the moment, the external folder contains the Google Test Framework only, which is added to cmake automatically.

## Authors

- [Pedro André Oliveira](https://www.linkedin.com/in/pedroandreoliveira/)

## Contributing

Contributions are always welcome!

## Related Knowledge Sharings

* [C++ Test-Driven Development](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git)
* [C++ Design Patterns KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git)
* [C++ Clean Architecture KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-clean-architecture.git)

* [C++ Fundamentals KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git)
* [C++ Intermediate KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-intermediate.git)
* [C++ Advanced KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git)
* [C++ Mastery KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git)

## References

* [C++ Programming Language](https://www.amazon.es/C-Programming-Language-Bjarne-Stroustrup/dp/0321958322)
* [Programming: Principles and Practice Using C++](https://www.amazon.es/Programming-Principles-Practice-Using-C/dp/0321992784)
* [Game Programming in C++: Creating 3D Games: Creating 3D Games (Game Design)](https://www.amazon.es/Game-Programming-Creating-Addison-Wesley-Development/dp/0134597206/)

### YouTube

* [TheCherno](https://www.youtube.com/c/TheChernoProject)
* [ChiliTomatoNoodle](https://www.youtube.com/c/ChiliTomatoNoodle)
* [Cᐩᐩ Weekly With Jason Turner](https://www.youtube.com/user/lefticus1)
* [CppCon](https://www.youtube.com/user/CppCon)
