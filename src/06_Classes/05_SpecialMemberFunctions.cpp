#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Special Member Functions
/// @sa 11_MoveSemantics
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Constructors
/// ---------------------------------------------------------------------------

class DefaultConstructorClass {
  public:
	DefaultConstructorClass() { puts("DefaultConstructorClass"); }
};

class CopyConstructorClass {
  public:
	CopyConstructorClass() { puts("CopyConstructorClass default ctor"); };
	CopyConstructorClass(const CopyConstructorClass &other) {
		puts("Copy constructor");
	}
};

/// ---------------------------------------------------------------------------
/// @subsection Destructors
/// ---------------------------------------------------------------------------

class DestructorClass {
  public:
	~DestructorClass() { puts("~DestructorClass()"); }
};

/// @subsubsection Initializing Lists

class ClassWithWrongInitList {
  public:
	explicit ClassWithWrongInitList(std::string aString)
		/*
			! THE ORDER OF THIS LIST MUST BE THE SAME AS IN THE MEMBER FIELDS
			! ... otherwise, it will not initialize correctly, therefore
			! ... in this case, swap the member fields declaration below
			! ... to prevent mStringSize to be initialized with the wrong size
		*/
		: mString(aString), mStringSize(mString.size()) {
		puts("ClassWithWrongInitList:: Explicit ctor()");
	}

	std::string getString() { return mString; }
	std::size_t getStringSize() { return mStringSize; }

  private:
	std::size_t mStringSize;
	std::string mString;
};

class ClassWithCorrectInitList {
  public:
	explicit ClassWithCorrectInitList(std::string aString)
		: mString(aString), mStringSize(mString.size()) {
		puts("ClassWithCorrectInitList Explicit ctor()");
	}

	std::string getString() { return mString; }
	std::size_t getStringSize() { return mStringSize; }

  private:
	std::string mString;
	std::size_t mStringSize;
};

/// ---------------------------------------------------------------------------
/// @subsection Copy Assignment
/// ---------------------------------------------------------------------------

class CopyAssignmentClass {
  public:
	CopyAssignmentClass() { puts("CopyAssignmentClass default ctor"); }

	CopyAssignmentClass(const CopyAssignmentClass &other) {
		puts("CopyAssignmentClass copy ctor");
	}

	CopyAssignmentClass &operator=(const CopyAssignmentClass &other) {
		puts("CopyAssignmentClass copy assignment");
		if (this != &other) {
			puts("copy something");
		}
		return *this;
	}
};

class SpecialClass {
  public:
	SpecialClass() { puts("SpecialClass ctor()"); }
	SpecialClass(const SpecialClass &other) { puts("SpecialClass copy ctor"); }
	SpecialClass(SpecialClass &&other) { puts("SpecialClass move ctor"); }

	SpecialClass &operator=(const SpecialClass &other) {
		puts("SpecialClass copy assignment");
		return *this;
	}
	SpecialClass &operator=(SpecialClass &&other) {
		puts("SpecialClass move assignment");
		return *this;
	}
	~SpecialClass() { puts("SpecialClass dtor()"); }
};

/// ===========================================================================
/// @section main
/// ===========================================================================

int main() {

	puts("============================================================");
	{ DefaultConstructorClass dcc; }

	puts("============================================================");
	{
		DestructorClass dc;
		puts("about to leave scope");
	}

	puts("============================================================");
	ClassWithWrongInitList cwwil("Hello World");
	std::cout << "getString():     " << cwwil.getString() << '\n';
	std::cout << "getStringSize(): " << cwwil.getStringSize() << '\n';

	puts("============================================================");
	ClassWithCorrectInitList cwcil("Hello World");
	std::cout << "getString():     " << cwcil.getString() << '\n';
	std::cout << "getStringSize(): " << cwcil.getStringSize() << '\n';

	puts("============================================================");
	CopyConstructorClass ccc1;
	CopyConstructorClass ccc2 = ccc1;

	puts("============================================================");
	CopyAssignmentClass cac1;
	CopyAssignmentClass cac2 = cac1;
	CopyAssignmentClass cac3;
	cac3 = cac1;

	/// -----------------------------------------------------------------------
	/// @brief All Special Members
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	{
		std::vector<SpecialClass> specialClasses;

		SpecialClass sc1;
		SpecialClass sc2 = sc1; // copy constructor
		SpecialClass sc3;
		sc3 = sc1; // copy assignment
		SpecialClass sc5;
		sc5 = std::move(sc2);
		specialClasses.emplace_back(std::move(sc1));
	}

	return EXIT_SUCCESS;
}
