#include "StandardLibrary.hpp"

struct SimpleStruct {
	int data;
};

class SimpleClass {
  public:
	int getData() { return data; }
	void setData(int newData) { data = newData; }

  private:
	int data;
};

/// ===========================================================================
/// @section main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section Classes and Structs
	/// @sa https://en.cppreference.com/w/cpp/language/classes
	/// @note class and struct equal except for its default visibility setting
	/// =======================================================================

	/// @note everything in a struct is **public** by default
	SimpleStruct aSimpleStruct;
	printf("SimpleStruct::data is %s\n", aSimpleStruct.data);

	/// @note everything in a struct is **private** by default
	SimpleClass aSimpleClass;
	aSimpleClass.setData(10);
	printf("SimpleClass::data is %d\n", aSimpleClass.getData());

	return EXIT_SUCCESS;
}
