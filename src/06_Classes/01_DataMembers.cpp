#include "StandardLibrary.hpp"

struct Birthday {
	int day;
	int month;
	int year;
};

struct Player {
	char firstName[32];
	char lastName[32];
	Birthday birthday;
};

/// ===========================================================================
/// @section main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section Data Members
	/// =======================================================================
	Player aPlayer{"John", "Doe", {7, 11, 1990}};
	printf("aPlayer data: {\n");
	printf("  full name: %s %s\n", aPlayer.firstName, aPlayer.lastName);
	printf("  age: %d\n", 2021 - aPlayer.birthday.year);
	puts("}");
    puts("");

	return EXIT_SUCCESS;
}
