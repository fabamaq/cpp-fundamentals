#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section RAII - Resource Acquisition Is Initialization
/// ===========================================================================

class FileRAII {
  public:
	FileRAII(const char *filename) {
		puts("FileRAII default ctor");
		puts("  opening the file");
		mFile.open(filename);
		puts("  file opened successfully");
	}

	void Write(const char *message) {
		puts("FileRAII::Write");
		puts("  Writing into file");
		mFile << message;
		puts("  Wrote successfully");
	}

	~FileRAII() {
		puts("FileRAII default dtor");
		puts("  closing the file");
		mFile.close();
		puts("  file closed successfully");
	}

  private:
	std::ofstream mFile;
};

/// ===========================================================================
/// @section Helper functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection returns a string of this file path thanks to the __FILE__ macro
/// ---------------------------------------------------------------------------
std::string get_file(const char *file) { return std::string(file); }
#define GET_THIS_FILE_PATH() get_file(__FILE__)

/// ---------------------------------------------------------------------------
/// @subsection returns the cwd by erasing "02_RAII.cpp" from this file path
/// ---------------------------------------------------------------------------
std::string get_cwd() {
	std::string thisFile = GET_THIS_FILE_PATH();
	thisFile.erase(thisFile.begin() + thisFile.find_last_of('/'),
				   thisFile.end());
	return thisFile;
}

std::string get_file_path(const char *filename) {
	std::string filepath = get_cwd();
	filepath.append("/");
	filepath.append(filename);
	return filepath;
}

/// ===========================================================================
/// @section main
/// ===========================================================================

int main() {

	{
		/// @note Constructor opens the file
		FileRAII aFile(get_file_path("example.txt").c_str());
		aFile.Write("Hello World");
		/// @note Destructor closes the file
	}

	try {
		FileRAII aFile(get_file_path("example2.txt").c_str());
		aFile.Write("Hello World");
    	printf("Erasing file example2.txt\n");
		remove(get_file_path("example2.txt").c_str());
	} catch (...) {
		printf("errno is: %d\n", errno);
		perror("Cannot erase file");
	}

	return EXIT_SUCCESS;
}
