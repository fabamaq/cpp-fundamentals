#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Inheritance
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Simple Inheritance
/// ---------------------------------------------------------------------------

/// @subsubsection Basic Example

class BaseClass {
  public:
	BaseClass() { puts("BaseClass ctor()"); }
	virtual ~BaseClass() { puts("BaseClass dtor()"); }
	virtual void foo() { puts("BaseClass::foo()"); }
};

class DerivedClass : public BaseClass {
  public:
	DerivedClass() { puts("DerivedClass ctor()"); }
	~DerivedClass() override { puts("DerivedClass dtor()"); }
	void foo() override { puts("DerivedClass::foo()"); }
};

/// @subsubsection Shape Example

class Shape {
  public:
	// Base class
	virtual ~Shape() = default;
	virtual void Draw() = 0;
	virtual double Area() = 0;
};

class Rectangle : public Shape {
  public:
	Rectangle(int width, int height) : mWidth(width), mHeight(height) {}
	void Draw() override { puts("Rectangle::Draw"); }
	double Area() override { return static_cast<double>(mWidth * mHeight); }

  private:
	int mWidth;
	int mHeight;
};

class Triangle : public Shape {
  public:
	Triangle(int base, int height) : mBase(base), mHeight(height) {}
	void Draw() override { puts("Triangle::Draw"); }
	double Area() override { return (mBase * mHeight) / 2.0; }

  private:
	int mBase;
	int mHeight;
};

class Circle : public Shape {
  public:
	Circle(double radius) : mRadius(radius) {}
	void Draw() override { puts("Circle::Draw"); }
	double Area() override { return M_PI * mRadius * mRadius; }

  private:
	double mRadius;
};

/// @subsubsection Animal Example

// Animal class (base)
class Animal {
	std::string _name;
	std::string _type;
	std::string _sound;
	// private constructor prevents construction of base class
	Animal() {}

  protected:
	// protected constructor for use by derived classes
	Animal(const std::string &n, const std::string &t, const std::string &s)
		: _name(n), _type(t), _sound(s) {}

  public:
	void speak() const;
	const std::string &name() const { return _name; }
	const std::string &type() const { return _type; }
	const std::string &sound() const { return _sound; }
};

void Animal::speak() const {
	printf("%s the %s says %s\n", _name.c_str(), _type.c_str(), _sound.c_str());
}

// Dog class - derived from Animal
class Dog : public Animal {
	int _walked;

  public:
	Dog(std::string n) : Animal(n, "dog", "woof"), _walked(0){};
	int walk() { return ++_walked; }
};

// Cat class - derived from Animal
class Cat : public Animal {
	int _petted;

  public:
	Cat(std::string n) : Animal(n, "cat", "meow"), _petted(0){};
	int pet() { return ++_petted; }
};

// Pig class - derived from Animal
class Pig : public Animal {
	int _fed;

  public:
	Pig(std::string n) : Animal(n, "pig", "oink"), _fed(0){};
	int feed() { return ++_fed; }
};

/// ---------------------------------------------------------------------------
/// @subsection Multiple Inheritance
/// ---------------------------------------------------------------------------

/// @subsubsection The Diamond Problem

class A {
  public:
	A() { puts("A:: ctor"); }
	virtual ~A() { puts("A:: dtor"); }

  protected:
	int value;
};

class B : public virtual A {
  public:
	B() { puts("B:: ctor"); }
	virtual ~B() { puts("B:: dtor"); }
};

class C : public virtual A {
  public:
	C() { puts("C:: ctor"); }
	virtual ~C() { puts("C:: dtor"); }
};

class D : public B, public C {
  public:
	D() { puts("D:: ctor"); }
	~D() override { puts("D:: dtor"); }
	void Print() { std::cout << "base::value is " << value << std::endl; }
};

/// ===========================================================================
/// @brief Application Entry Point
/// ===========================================================================
int main() {

	/// -----------------------------------------------------------------------
	/// @subsection Simple Inheritance
	/// -----------------------------------------------------------------------
	puts("==================================================================");

	/// @subsubsection Basic Example

	{
		puts("BaseClass bc;");
		BaseClass bc;

		puts("bc.foo();");
		bc.foo();

		puts("DerivedClass dc;");
		DerivedClass dc;

		puts("dc.foo();");
		dc.foo();

		puts("BaseClass *pBc = new BaseClass();");
		BaseClass *pBc = new BaseClass();

		puts("pBc->foo();");
		pBc->foo();

		puts("BaseClass *pDc = new DerivedClass();");
		BaseClass *pDc = new DerivedClass();

		puts("pDc->foo();");
		pDc->foo();

		puts("delete pBc;");
		delete pBc;
		puts("delete pDc;");
		delete pDc;
	}

	puts("==================================================================");
	/// @subsubsection Shape Example

	{

		Rectangle rect(16, 10);
		rect.Draw();
		std::cout << "Area of a rectangle is " << rect.Area() << std::endl;

		Triangle tri(7, 3);
		tri.Draw();
		std::cout << "Area of a triangle is " << tri.Area() << std::endl;

		Circle cir(10);
		cir.Draw();
		std::cout << "Area of a circle is " << cir.Area() << std::endl;
	}

	puts("==================================================================");
	/// @subsubsection Animal Example

	{
		Dog d("Rover");
		Cat c("Fluffy");
		Pig p("Arnold");

		d.speak();
		c.speak();
		p.speak();

		std::cout << "the " << d.type() << " has been walked " << d.walk()
				  << " times" << std::endl;
		std::cout << "the " << c.type() << " has been petted " << c.pet()
				  << " times" << std::endl;
		std::cout << "the " << p.type() << " has been fed " << p.feed()
				  << " times" << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Multiple Inheritance
	/// -----------------------------------------------------------------------
	puts("==================================================================");

	/// @subsubsection The Diamond Problem

	{
		puts("A *pB = new B();");
		A *pB = new B();

		puts("A *pC = new C();");
		A *pC = new C();

		puts("A *pD = new D();");
		A *pD = new D();

		puts("delete pB");
		delete pB;
		puts("delete pC");
		delete pC;
		puts("delete pD");
		delete pD;
	}

	return 0;
}
