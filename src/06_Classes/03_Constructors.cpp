#include "StandardLibrary.hpp"

struct Birthday {
	int day;
	int month;
	int year;

	/// @note this disables the compiler-generated aggregate constructor
	/// ... and forces the developer to specifiy the three values for birthday
	Birthday(int d, int m, int y) : day(d), month(m), year(y) {}
};

class Player {
	/// @subsubsection Data Members
	std::string firstName{};
	std::string lastName{};
	Birthday birthday;

  public:
	/// @subsubsection Member Functions
	// Constructor
	Player(std::string aFirstName, std::string aLastName, Birthday aBirthday)
		: firstName(aFirstName), lastName(aLastName), birthday(aBirthday) {}

	void Print() {
		printf("{\n");
		printf("  full name: %s %s\n", firstName.c_str(), lastName.c_str());
		printf("  age: %d\n", 2021 - birthday.year);
		puts("}");
		puts("");
	}

	Birthday getBirthday() { return birthday; }

	void setLastName(std::string newLastName) { lastName = newLastName; }
	void setBirthday(Birthday newBirthday) { birthday = newBirthday; }
};

/// ===========================================================================
/// @section main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section Constructors
	/// =======================================================================
	Player playerOne{"John", "Doe", {7, 11, 1990}};
	playerOne.Print();
	playerOne.setLastName("Wick");
	Birthday newBirthday{12, 9, 1964};
	playerOne.setBirthday(newBirthday);
	playerOne.Print();

	Player playerTwo{"Kyle", "Wick", playerOne.getBirthday()};
	playerTwo.Print();

	return EXIT_SUCCESS;
}
