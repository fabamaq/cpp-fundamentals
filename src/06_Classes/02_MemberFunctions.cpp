#include "StandardLibrary.hpp"

struct Birthday {
	int day;
	int month;
	int year;
};

struct Player {
	/// @subsubsection Data Members
	std::string firstName{};
	std::string lastName{};
	Birthday birthday;

	/// @subsubsection Member Functions
	void Print() {
		printf("{\n");
		printf("  full name: %s %s\n", firstName.c_str(), lastName.c_str());
		printf("  age: %d\n", 2021 - birthday.year);
		puts("}");
		puts("");
	}

	void setLastName(std::string newLastName) { lastName = newLastName; }
	void setBirthday(Birthday newBirthday) { birthday = newBirthday; }
};

/// ===========================================================================
/// @section main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section Member Functions
	/// =======================================================================
	Player aPlayer{"John", "Doe", {7, 11, 1990}};
	aPlayer.Print();
	aPlayer.setLastName("Wick");
	Birthday newBirthday{12, 9, 1964};
	aPlayer.setBirthday(newBirthday);
	aPlayer.Print();

	return EXIT_SUCCESS;
}
