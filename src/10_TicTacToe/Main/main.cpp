#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Global Data
/// ===========================================================================

constexpr unsigned int ROWS = 3;
constexpr unsigned int COLS = 3;

constexpr char playerChar = 'X';
constexpr char computerChar = 'O';

/// ===========================================================================
/// @section Global Functions
/// ===========================================================================

void PrintBoard(char uiBoard[ROWS][COLS]) {
	printf("   BOARD \n");
	for (auto row = 0U; row < ROWS; ++row) {
		printf(" [ ");
		for (auto col = 0U; col < COLS; ++col) {
			printf("%c ", uiBoard[row][col]);
		}
		puts("]");
	}
	puts("");
}

// clang-format off
bool IsGameOver(char board[ROWS][COLS]) {
	// Horizontal Check
	if (board[0][0] == board[0][1] && board[0][0] == board[0][2]) return true;
	if (board[1][0] == board[1][1] && board[1][0] == board[1][2]) return true;
	if (board[2][0] == board[2][1] && board[2][0] == board[2][2]) return true;
	// Vertical Check
	if (board[0][0] == board[1][0] && board[0][0] == board[2][0]) return true;
	if (board[0][1] == board[1][1] && board[0][1] == board[2][1]) return true;
	if (board[0][2] == board[1][2] && board[0][2] == board[2][2]) return true;
	// Diagonal Check
	if (board[0][0] == board[1][1] && board[0][0] == board[2][2]) return true;
	if (board[0][2] == board[1][1] && board[0][2] == board[2][0]) return true;
	return false;
}
// clang-format on

void UpdateBoard(unsigned int pick, char board[ROWS][COLS], char newCharacter) {
	unsigned int row = pick / ROWS;
	unsigned int col = pick % COLS;
	board[row][col] = newCharacter;
}

/// ===========================================================================
/// @section Main - Game Entry Point
/// ===========================================================================
int main() {
	puts("Welcome to Tic Tac Toe");

	char uiBoard[COLS][ROWS] = {
		{'1', '2', '3'}, //
		{'4', '5', '6'}, //
		{'7', '8', '9'}, //
	};
	PrintBoard(uiBoard);

	std::vector<unsigned int> freeSpots = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	/// @subsection Choose first player

	// seed the random number generation
	srand(static_cast<unsigned int>(time(nullptr)));
	rand(); // burn the first random number to remove bias and patterns
	bool isPlayersTurn = bool(rand() % 2);

	/// @subsection Game Loop
	while (!freeSpots.empty()) {

		// player's turn
		if (isPlayersTurn) {
			puts("Your turn");
			/// @subsubsection Process Input
			unsigned int pick;
			std::string input;
			do {
				std::puts("Pick a number from the board (or 0 to quit)");
				std::cin >> input;
				std::cin.ignore();

				try {
					pick = static_cast<unsigned int>(std::stoul(input));
				} catch (...) {
					std::cerr << "Invalid input! Try again!" << std::endl;
					continue;
				}

				if (pick == 0) {
					puts("Quiting");
					return EXIT_SUCCESS;
				}
				auto iter = std::find(freeSpots.begin(), freeSpots.end(), pick);
				if (iter == freeSpots.end()) {
					puts("Invalid number");
				} else {
					freeSpots.erase(
						std::remove(freeSpots.begin(), freeSpots.end(), pick));
					break;
				}
			} while (true);

			/// @subsubsection Update
			UpdateBoard(--pick, uiBoard, playerChar);
			isPlayersTurn = !isPlayersTurn;

			if (IsGameOver(uiBoard)) {
				PrintBoard(uiBoard);
				puts("You win!");
				return EXIT_SUCCESS;
			}

		} else {
			// computer's turn
			puts("My turn");
			/// @subsubsection Process AI
			std::default_random_engine dre{};
			std::shuffle(freeSpots.begin(), freeSpots.end(), dre);
			auto pick = freeSpots.back();
			freeSpots.pop_back();
			std::sort(freeSpots.begin(), freeSpots.end());
			printf("I play %d\n", pick);

			/// @subsubsection Update
			UpdateBoard(--pick, uiBoard, computerChar);
			isPlayersTurn = !isPlayersTurn;

			if (IsGameOver(uiBoard)) {
				PrintBoard(uiBoard);
				puts("I win!");
				return EXIT_SUCCESS;
			}
		}
		/// @subsubsection render
		PrintBoard(uiBoard);
	}

	puts("It's a tie!");

	return EXIT_SUCCESS;
}
