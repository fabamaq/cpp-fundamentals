add_executable(TicTacToe)

target_sources(TicTacToe PRIVATE Main/main.cpp)

target_include_directories(
  TicTacToe PRIVATE ${ROOT_DIR}/include/CppStandardLibrary
                    ${ROOT_DIR}/src/TicTacToe
)

target_link_libraries(
  TicTacToe PRIVATE project_global_settings
                    global_pre_compiled_headers
)
