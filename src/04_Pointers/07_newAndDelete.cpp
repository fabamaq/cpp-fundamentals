#include <iostream>

using namespace std;

int main() {

	int *pointer(new int(55)); // define a pointer to an address that holds a 55
	cout << *pointer << endl;
	 /// ! don't forget to release the memory
	delete pointer;

	int *pArray(new int[5]{10, 20, 30, 40, 50});
	*(pArray + 1) += 5;
	cout << *pArray << ", " << *(pArray + 1) << endl;
	 /// ! don't forget to release the memory
	 /// @note the use of '[]' to delete arrays
	delete[] pArray;

	return 0;
}
