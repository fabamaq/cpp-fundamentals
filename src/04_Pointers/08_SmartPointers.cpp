#include <cassert>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <type_traits>

/******************************************************************************
 * @warning We are going to use advanced features for this example, which is
 * ... taken from the cppreference website
 *****************************************************************************/

/// @note polyfill code std::make_unique, supported only from c++14 and higher
namespace fmq {

	using namespace std;

	template <typename _Tp> struct _MakeUniq {
		typedef unique_ptr<_Tp> __single_object;
	};

	template <typename _Tp> struct _MakeUniq<_Tp[]> {
		typedef unique_ptr<_Tp[]> __array;
	};

	template <typename _Tp, size_t _Bound> struct _MakeUniq<_Tp[_Bound]> {
		struct __invalid_type {};
	};

	/// std::make_unique for single objects
	template <typename _Tp, typename... _Args>
	inline typename _MakeUniq<_Tp>::__single_object
	make_unique(_Args &&...__args) {
		return unique_ptr<_Tp>(new _Tp(std::forward<_Args>(__args)...));
	}

	/// std::make_unique for arrays of unknown bound
	template <typename _Tp>
	inline typename _MakeUniq<_Tp>::__array make_unique(size_t __num) {
		using type_without_extent = typename remove_extent<_Tp>::type;
		return unique_ptr<_Tp>(new type_without_extent[__num]());
	}

	/// Disable std::make_unique for arrays of known bound
	template <typename _Tp, typename... _Args>
	inline typename _MakeUniq<_Tp>::__invalid_type
	make_unique(_Args &&...) = delete;

} // namespace fmq

/// ---------------------------------------------------------------------------
/// @subsection unique_ptr
/// @sa https://en.cppreference.com/w/cpp/memory/unique_ptr
/// ---------------------------------------------------------------------------

// helper class for runtime polymorphism demo below
struct B {
	virtual ~B() = default;

	virtual void bar() { std::cout << "B::bar\n"; }
};

/// @brief class D inherits from class B.
/// @sa 06_Classes/07_Inheritance.cpp for more information about inheritance
struct D : B {
	D() { std::cout << "D::D\n"; }
	~D() { std::cout << "D::~D\n"; }

	void bar() override { std::cout << "D::bar\n"; }
};

// a function consuming a unique_ptr can take it by value or by rvalue reference
std::unique_ptr<D> pass_through(std::unique_ptr<D> p) {
	p->bar();
	return p;
}

// helper function for the custom deleter demo below
void close_file(std::FILE *fp) { std::fclose(fp); }

// unique_ptr-based linked list demo
struct List {
	struct Node {
		int data;
		std::unique_ptr<Node> next;
	};

	std::unique_ptr<Node> head;

	~List() {
		// destroy list nodes sequentially in a loop, the default destructor
		// would have invoked its `next`'s destructor recursively, which would
		// cause stack overflow for sufficiently large lists.
		while (head)
			head = std::move(head->next);
	}

	void push(int data) {
		head = std::unique_ptr<Node>(new Node{data, std::move(head)});
	}
};
/// ---------------------------------------------------------------------------
/// @subsection shared_ptr
/// @sa https://en.cppreference.com/w/cpp/memory/shared_ptr
/// @note this example uses advanced features from the Thread Support Library
/// ---------------------------------------------------------------------------
#include <chrono> // C++ time utilities
#include <mutex>  // Mutual exclusion primitives
#include <thread> // std::thread class and supporting functions

struct Base {
	Base() { std::cout << "  Base::Base()\n"; }
	// Note: non-virtual destructor is OK here
	~Base() { std::cout << "  Base::~Base()\n"; }
};

/// @brief class D inherits from class B.
/// @sa 06_Classes/07_Inheritance.cpp for more information about inheritance
struct Derived : public Base {
	Derived() { std::cout << "  Derived::Derived()\n"; }
	~Derived() { std::cout << "  Derived::~Derived()\n"; }
};

void thr(std::shared_ptr<Base> p) {
	std::this_thread::sleep_for(std::chrono::seconds(1));
	std::shared_ptr<Base> lp = p; // thread-safe, even though the
								  // shared use_count is incremented
	{
		static std::mutex io_mutex;
		std::lock_guard<std::mutex> lk(io_mutex);
		std::cout << "local pointer in a thread:\n"
				  << "  lp.get() = " << lp.get()
				  << ", lp.use_count() = " << lp.use_count() << '\n';
	}
}

/// ---------------------------------------------------------------------------
/// @subsection weak_ptr
/// @sa https://en.cppreference.com/w/cpp/memory/weak_ptr
/// ---------------------------------------------------------------------------

/// @note std::weak_ptr is a smart pointer that holds a non-owning ("weak")
/// reference to an object that is managed by std::shared_ptr.
std::weak_ptr<int> gw;

void observe() {
	std::cout << "gw.use_count() == " << gw.use_count() << "; ";
	// we have to make a copy of shared pointer before usage:
	if (std::shared_ptr<int> spt = gw.lock()) {
		std::cout << "*spt == " << *spt << '\n';
	} else {
		std::cout << "gw is expired\n";
	}
}

int main() {
	/// -----------------------------------------------------------------------
	/// @subsection unique_ptr
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	std::cout << "1) Unique ownership semantics demo\n";
	{
		// Create a (uniquely owned) resource
		std::unique_ptr<D> p = fmq::make_unique<D>();

		// Transfer ownership to `pass_through`,
		// which in turn transfers ownership back through the return value
		std::unique_ptr<D> q = pass_through(std::move(p));
		/// @note std::move is related to Move Semantics

		// `p` is now in a moved-from 'empty' state, equal to `nullptr`
		assert(!p);
	}

	std::cout << "\n"
				 "2) Runtime polymorphism demo\n";
	{
		// Create a derived resource and point to it via base type
		std::unique_ptr<B> p = fmq::make_unique<D>();

		// Dynamic dispatch works as expected
		p->bar();
	}

	std::cout << "\n"
				 "3) Custom deleter demo\n";
	std::ofstream("demo.txt") << 'x'; // prepare the file to read
	{
		using unique_file_t = std::unique_ptr<std::FILE, decltype(&close_file)>;
		/// @note create a unique_ptr from the file handle returned from fopen
		/// ... and use `close_file` as the deleter function to call when this
		/// ... unique_ptr goes out of scope '{}'
		unique_file_t fp(std::fopen("demo.txt", "r"), &close_file);
		if (fp)
			std::cout << char(std::fgetc(fp.get())) << '\n';
	} // `close_file()` called here (if `fp` is not null)

	/*
		This final example uses a lambda function as the deleter function.
		Lambda functions appear in 05_Functions/06_LambdaFunctions.

		Another very important concept used in this example is RAII, which
		appears in 06_Clases/06_RAII.cpp, and tells us that the deleter function
		will be called even if some exception is thrown.
	 */
	std::cout
		<< "\n"
		   "4) Custom lambda-expression deleter and exception safety demo\n";
	try {
		std::unique_ptr<D, void (*)(D *)> p(new D, [](D *ptr) {
			std::cout << "destroying from a custom deleter...\n";
			delete ptr;
		});

		throw std::runtime_error(
			""); // `p` would leak here if it were instead a plain pointer
	} catch (const std::exception &) {
		std::cout << "Caught exception\n";
	}

	std::cout << "\n"
				 "5) Array form of unique_ptr demo\n";
	{ std::unique_ptr<D[]> p(new D[3]); } // `D::~D()` is called 3 times

	std::cout << "\n"
				 "6) Linked list demo\n";
	{
		List wall;
		for (int beer = 0; beer != 1000000; ++beer)
			wall.push(beer);

		std::cout << "1'000'000 bottles of beer on the wall...\n";
	} // destroys all the beers

	/// -----------------------------------------------------------------------
	/// @subsection shared_ptr
	/// -----------------------------------------------------------------------
	puts("==================================================================");

	std::shared_ptr<Base> p = std::make_shared<Derived>();

	std::cout << "Created a shared Derived (as a pointer to Base)\n"
			  << "  p.get() = " << p.get()
			  << ", p.use_count() = " << p.use_count() << '\n';
	std::thread t1(thr, p), t2(thr, p), t3(thr, p);
	p.reset(); // release ownership from main
	std::cout << "Shared ownership between 3 threads and released\n"
			  << "ownership from main:\n"
			  << "  p.get() = " << p.get()
			  << ", p.use_count() = " << p.use_count() << '\n';
	t1.join();
	t2.join();
	t3.join();
	std::cout << "All threads completed, the last one deleted Derived\n";

	/// -----------------------------------------------------------------------
	/// @subsection weak_ptr
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	{
		auto sp = std::make_shared<int>(42);
		gw = sp;

		observe();
	}

	observe();

	return 0;
}
