#include <iostream>

using namespace std;

double averageCost(double *priceArray, int count) {
	double sum = 0;
	for (int i = 0; i < count; i++) {
		sum += *(priceArray + i);
	}
	return sum / count;
}

/// @note function overload
double averageCost(double *priceArray, int *count) {
	double sum = 0;
	for (int i = 0; i < *count; i++) {
		sum += *(priceArray + i);
	}
	double avg = sum / *count;
	*count += 5;
	cout << "In function count value: " << *count << endl;
	return avg;
}

int main() {
	double prices[5]{5.00, 4.50, 3.75, 3.10, 6.75};
	double average = averageCost(prices, 5);
	cout << "$" << average << endl;

	int quantity = 5;
	double avg = averageCost(prices, &quantity);
	cout << "$" << avg << endl;
	cout << "quantity value: " << quantity << endl;

	return 0;
}
