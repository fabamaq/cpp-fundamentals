# C++ Pointers

## Pointers Defined

> * Pointers are like mailboxes.
> * Every mailbox has an address.
> * Inside every mailbox, there may be an object (e.g.: a letter).
> * The memory location is called an `address`.
> * Memory locations are numbered sequentially.
> * Address are in hexadecimal notation, e.g., 0x01AC4D.
> * Addresses change from execution to execution.
> * A `pointer` is a `variable` that stores an `address` location.
> * Pointer notation is faster when working with `arrays`.
> * They provide `functions` access to large blocks of `data`.
> * They can `allocate` memory dynamically.
> * When a `pointer` is not initialized, it is set to ~~`NULL`~~ **`nullptr`**.
> * `nullptr` sets the `pointer` to all zeroes.
> * It is always a good idea to initialize a pointer.
> * It is helpful to use a prefix of "p" for pointer variable names.

## Address vs. pointers


```cpp
/// @file address.cpp
#include <iostream>

using namespace std;

int main() {
	int number = 240; // define an int variable
	int *numPtr;	  // define an integer pointer numPtr
	numPtr = &number; // assign the address to numPtr
	cout << "The address of number is: " << numPtr << endl;

	return 0;
}
```

## Pointer memory

### Memory Allocation

| Data Type | Memory Allocated |
| :-: | :-: |
| char, bool | 1 byte |
| short | 2 byte |
| int, long, float | 4 byte |
| double | 8 byte |


```cpp
/// @file memory.cpp
#include <iostream>

using namespace std;

int main() {
	cout << "Size of boolean: " << sizeof(bool) << endl;
	cout << "Size of char: " << sizeof(char) << endl;
	cout << "Size of int: " << sizeof(int) << endl;
	cout << "Size of float: " << sizeof(float) << endl;
	cout << "Size of long: " << sizeof(long) << endl;
	cout << "Size of double: " << sizeof(double) << endl;

	bool *pBool;
	char *pChar;
	int *pInt;
	cout << "Size of boolean ptr: " << sizeof(pBool) << endl;
	cout << "Size of char ptr: " << sizeof(pChar) << endl;
	cout << "Size of int ptr: " << sizeof(pInt) << endl;

	return 0;
}
```

### Dynamic Memory Allocation

> * Defines data requirements at runtime, rather than defining a predefined amount of memory.
> * Makes program flexible to request only needed memory.
> * Dynamic memory is identified by its address.
> * Pointers hold the address for this type of memory.
> * Memory can also be released when it is not needed.
> * Memory can be increased or decreased.
> * `Errors` in your code can cause **`memory leaks`**.
> * You should watch out for **`memory fragmentation`**.

### Stack and Heap

> * Function arguments are stored on the `stack`.
> * The return location is also stored on the stack.
> * Memory not used by the OS or programs is called the `heap`.
> * This space can be used for dynamic variable creation.
> * The `new` operator is used for dynamic memory.
> * This returns the address of the space stored in a pointer.
> * The complement is the `delete` operator.
> * Delete releases the space.
