#include <iostream>
#include <string>

/// ===========================================================================
/// @section Template Functions
/// ===========================================================================

/**
 * @brief Returns the maximum between two objects of type T
 *
 * @tparam T - the type of the parameters
 * @param a - the left hand side parameter
 * @param b - the right hand side parameter
 *
 * @return the max of a and b in accordance to the type
 */
template <typename T>
//
T maxof(T a, T b) {
	return (a > b ? a : b);
}

/// ===========================================================================
/// @section main - application entry point
/// ===========================================================================
int main() {

	// this will create a function of type "int maxof(int a, int b) ... "
	std::cout << maxof<int>(7, 9) << std::endl;

	// this will create a "string maxof(string a, string b) ... function "
	std::cout << maxof<std::string>("seven", "nine") << std::endl;

	return EXIT_SUCCESS;
}
