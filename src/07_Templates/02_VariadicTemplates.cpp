#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Helper Functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Template Operator<<
/// ---------------------------------------------------------------------------

/// @subsubsection vector specialization

template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// ===========================================================================
/// @section Variadic Template (c++ 11 or higher)
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection variadic template function
/// ---------------------------------------------------------------------------

/// @subsubsection base template (if no specialization is better, this runs)
template <typename... T> void Print(T... args) {}

/// @subsubsection template specialization

template <typename FirstType, typename... OtherTypes>
void Print(FirstType firstArg, OtherTypes... args) {
	std::cout << firstArg << std::endl;
	Print(args...);
}

template <class T>
void Print(const std::string name, const std::vector<T> &data) {
	std::cout << name << ": [ " << data << "]" << std::endl;
}

// // template <class T>
// // void Print(const char* name, const std::vector<T> &data) {
// // 	std::cout << name << ": [ " << data << "]" << std::endl;
// // }

/// ---------------------------------------------------------------------------
/// @subsection variadic template class
/// ---------------------------------------------------------------------------

template <typename... Ts> class Tuple {};

template <typename T> int f(Tuple<T *>) { return 1; }

template <typename... Ts> int f(Tuple<Ts...>) { return 2; }

template <typename... Ts> int f(Tuple<Ts *...>) { return 3; }

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================
int main() {

	/// @subsection variadic template function

	{
		Print<int, double, char, float, std::string>(42, 9.8, '$', 3.14f,
													 "Thanks");

		std::vector<int> numbers{1, 2, 3, 4, 5, 6, 7};

		Print("numbers", numbers); // prints 1 2 3 4 5 6 7

		// ? why doesn't it print numbers: [ 1 2 3 4 5 6 7 ] ?
	}

	/// @subsection variadic template class

	{
		/**
			Since Tuple is a Variadic Template Class, the compiler must resolve
			the overloads for all three function `f` so the runtime knows which
			function to call

			@sa 05_Functions/01_Overloads.cpp
		*/
		std::cout << f(Tuple<int, double>());	  // calls f<>(Tuple<Ts...>)
		std::cout << f(Tuple<int *, double *>()); // calls f<>(Tuple<Ts*...>)
		std::cout << f(Tuple<int *>());			  // calls f<>(Tuple<T*>)
		std::cout << std::endl;
	}

	return EXIT_SUCCESS;
}

/// @note more on Templates in the intermediate, advanced and mastery KS
