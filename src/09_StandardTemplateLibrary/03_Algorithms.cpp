#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Helper Functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Template Operator<<
/// ---------------------------------------------------------------------------

/// @subsubsection partial specialization for vector<T>
template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection full specialization for vector<bool>
std::ostream &operator<<(std::ostream &os, const std::vector<bool> &data) {
	for (const auto &value : data) {
		os << (value ? "T" : "F") << ' ';
	}
	return os;
}

/// ---------------------------------------------------------------------------
/// @subsection Template Print Functions
/// ---------------------------------------------------------------------------

/// @subsubsection vector specialization
template <class T>
void Print(const std::string name, const std::vector<T> &data) {
	std::cout << name << ": [ " << data << "]" << std::endl;
}

/// ---------------------------------------------------------------------------
/// @subsection Template is_prime
/// ---------------------------------------------------------------------------

template <typename T> const bool is_prime(const T &num) {
	if (num <= 1)
		return false;
	bool primeflag = true;
	for (T l = 2; l < num; ++l) {
		if (num % l == 0) {
			primeflag = false;
			break;
		}
	}
	return primeflag;
}

/// ---------------------------------------------------------------------------
/// @subsection Template is_odd and is_even
/// ---------------------------------------------------------------------------

template <typename T> bool is_odd(const T &n) { return ((n % 2) == 1); }
template <typename T> bool is_even(const T &n) { return ((n % 2) == 0); }
template <typename T> bool is_even_tens(T &n) {
	if (n < 10) {
		return false;
	}
	return ((n / 10) % 2) == 0;
}

/// ---------------------------------------------------------------------------
/// @subsection Sorting Predicate Functions
/// ---------------------------------------------------------------------------
template <typename T> bool mycomp(const T &lh, const T &rh) {
	return int(lh) < int(rh);
}

// compare for reverse sort
template <typename T> bool gtcomp(const T &lh, const T &rh) { return lh > rh; }

/// ---------------------------------------------------------------------------
/// @subsection Random Number Generator
/// ---------------------------------------------------------------------------

struct RNG {
	int operator()() {
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> distrib(min, max);
		return distrib(gen);
	}

	int min{};
	int max{};
};

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section STL Algorithms
	/// @sa https://en.cppreference.com/w/cpp/header/algorithm
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection Non-modifying sequence operations
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	puts("Non-modifying sequence operations");

	/// @subsubsection all_of
	{
		puts("------------------------------------------------------- all_of");
		// prime numbers < 100
		const std::vector<int> v1 = {2,	 3,	 5,	 7,	 11, 13, 17, 19, 23,
									 29, 31, 37, 41, 43, 47, 53, 59, 61,
									 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		if (all_of(v1.begin(), v1.end(), is_prime<int>)) {
			std::cout << "all numbers are prime" << std::endl;
		} else {
			std::cout << "not all numbers are prime" << std::endl;
		}
	}

	/// @subsubsection any_of

	/// @subsubsection none_of

	/// @subsubsection find
	{
		puts("--------------------------------------------------------- find");
		// prime numbers < 100
		const std::vector<int> v1 = {2,	 3,	 5,	 7,	 11, 13, 17, 19, 23,
									 29, 31, 37, 41, 43, 47, 53, 59, 61,
									 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		auto it = find(v1.begin(), v1.end(), 41);

		if (it != v1.end()) {
			size_t index = it - v1.begin();
			std::cout << "found " << *it << " at index " << index << '\n';
		} else {
			std::cout << "not found" << std::endl;
		}
	}

	/// @subsubsection find_if
	{
		puts("------------------------------------------------------ find_if");
		// prime numbers < 100
		const std::vector<int> v1 = {2,	 3,	 5,	 7,	 11, 13, 17, 19, 23,
									 29, 31, 37, 41, 43, 47, 53, 59, 61,
									 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		auto it = find_if(v1.begin(), v1.end(), is_odd<int>);

		if (it != v1.end()) {
			size_t index = it - v1.begin();
			std::cout << "found " << *it << " at index " << index << '\n';
		} else {
			std::cout << "not found" << std::endl;
		}
	}

	/// @subsubsection search
	{
		puts("------------------------------------------------------- search");
		// prime numbers < 100
		const std::vector<int> v1 = {2,	 3,	 5,	 7,	 11, 13, 17, 19, 23,
									 29, 31, 37, 41, 43, 47, 53, 59, 61,
									 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);
		const std::vector<int> v2 = {31, 37, 41, 43, 47};
		Print("v2", v2);

		auto it = search(v1.begin(), v1.end(), v2.begin(), v2.end());

		if (it != v1.end()) {
			size_t index = it - v1.begin();
			std::cout << "found " << *it << " at index " << index << '\n';
		} else {
			std::cout << "not found" << std::endl;
		}
	}

	/// @subsubsection count
	{
		puts("-------------------------------------------------------- count");
		// prime numbers < 100
		const std::vector<int> v1 = {2,	 3,	 5,	 7,	 11, 13, 17, 19, 23,
									 29, 31, 37, 41, 43, 47, 53, 59, 61,
									 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		auto c = std::count(v1.begin(), v1.end(), 7);

		std::cout << "found " << c << " occurance(s) of 7" << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Modifying sequence operations
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	puts("Modifying sequence operations");

	/// @subsubsection replace
	{
		puts("------------------------------------------------------ replace");
		// prime numbers < 100
		std::vector<int> v1 = {2,  3,  5,  7,  11, 13, 17, 19, 23,
							   29, 31, 37, 41, 43, 47, 53, 59, 61,
							   67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		std::replace(v1.begin(), v1.end(), 47, 99);
		Print("v1", v1);

		std::replace_if(v1.begin(), v1.end(), is_even<int>, 99);
		Print("v1", v1);
	}

	/// @subsubsection remove
	{
		puts("------------------------------------------------------- remove");
		// prime numbers < 100
		std::vector<int> v1 = {2,  3,  5,  7,  11, 13, 17, 19, 23,
							   29, 31, 37, 41, 43, 47, 53, 59, 61,
							   67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		auto it = std::remove(v1.begin(), v1.end(), 43);
		if (it == v1.end()) {
			std::cout << "no elements were removed" << std::endl;
		} else {
			v1.resize(it - v1.begin());
		}
		Print("v1", v1);

		it = std::remove_if(v1.begin(), v1.end(), is_even<int>);
		if (it == v1.end()) {
			std::cout << "no elements were removed" << std::endl;
		} else {
			v1.resize(it - v1.begin());
		}
		Print("v1", v1);
	}

	/// @subsubsection unique
	{
		puts("------------------------------------------------------- unique");
		// prime numbers < 100
		std::vector<int> v1 = {2,  3,  5,  7,  11, 13, 17, 19, 23,
							   29, 31, 37, 41, 42, 42, 53, 59, 61,
							   67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		auto it = std::unique(v1.begin(), v1.end());
		if (it == v1.end()) {
			std::cout << "no elements were removed" << std::endl;
		} else {
			v1.resize(it - v1.begin());
		}
		Print("v1", v1);
	}

	/// @subsubsection copy
	{
		puts("--------------------------------------------------------- copy");
		// prime numbers < 100
		std::vector<int> v1 = {2,  3,  5,  7,  11, 13, 17, 19, 23,
							   29, 31, 37, 41, 43, 47, 53, 59, 61,
							   67, 71, 73, 79, 83, 89, 97};
		std::vector<int> v2(v1.size(), 0);
		Print("v1", v1);
		Print("v2", v2);

		/// @brief copy_n
		std::copy(v1.begin(), v1.end(), v2.begin());
		Print("v2", v2);

		/// @brief copy_n
		std::vector<int> v3(v1.size(), 0);
		Print("v3", v3);
		std::copy_n(v1.begin(), 15, v3.begin());
		Print("v3", v3);

		/// @brief copy_backward
		std::vector<int> v4(v1.size(), 0);
		Print("v4", v4);
		std::copy_backward(v1.begin(), v1.end(), v4.end());
		Print("v4", v4);

		/// @brief reverse_copy
		std::vector<int> v5(v1.size(), 0);
		Print("v5", v5);
		std::reverse_copy(v1.begin(), v1.end(), v5.begin());
		Print("v5", v5);

		/// @brief reverse
		Print("v1", v1);
		std::reverse(v1.begin(), v1.end());
		Print("v1", v1);

		/// @brief fill_n
		Print("v1", v1);
		std::fill_n(v1.begin(), 21, 42);
		Print("v1", v1);

		/// @brief generate
		Print("v2", v2);
		RNG rng{1, 100};
		std::generate(v2.begin(), v2.end(), rng);
		Print("v2", v2);

		/// @brief shuffle
		Print("v5", v5);
		std::default_random_engine dre;
		std::shuffle(v5.begin(), v5.end(), dre);
		Print("v5", v5);
	}

	/// -----------------------------------------------------------------------
	/// @subsection Partition operations
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	puts("Partition operations");

	/// @subsubsection partition
	{
		puts("---------------------------------------------------- partition");
		// prime numbers > 10 & < 100
		std::vector<int> v1 = {11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47,
							   53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		std::partition(v1.begin(), v1.end(), is_even_tens<int>);
		Print("v1", v1);
	}

	/// @subsubsection stable_partition
	{
		puts("--------------------------------------------- stable_partition");
		// prime numbers > 10 & < 100
		std::vector<int> v1 = {11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47,
							   53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		std::stable_partition(v1.begin(), v1.end(), is_even_tens<int>);
		Print("v1", v1);
	}

	/// @subsubsection partition_copy
	{
		puts("----------------------------------------------- partition_copy");
		// prime numbers > 10 & < 100
		std::vector<int> v1 = {11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47,
							   53, 59, 61, 67, 71, 73, 79, 83, 89, 97};
		Print("v1", v1);

		std::size_t sz = std::count_if(v1.begin(), v1.end(), is_even_tens<int>);
		std::vector<int> v2(sz), v3(v1.size() - sz);

		std::partition_copy(v1.begin(), v1.end(), v2.begin(), v3.begin(),
							is_even_tens<int>);

		Print("v1", v1);
		Print("v2", v2);
		Print("v3", v3);
	}

	/// -----------------------------------------------------------------------
	/// @subsection Sorting Operations
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	puts("Sorting operations");

	/// @subsubsection sort
	{
		puts("--------------------------------------------------------- sort");
		// prime numbers < 100
		std::vector<int> v1 = {83, 53, 47, 23, 13, 59, 29, 41, 19,
							   71, 31, 67, 11, 2,  97, 7,  61, 73,
							   3,  79, 37, 43, 17, 5,  89};
		std::vector<double> v2 = {3.07, 2.49, 3.73, 6.58, 3.3,	2.72, 3.44,
								  8.78, 9.23, 4.09, 4.4,  1.65, 4.92, 0.42,
								  4.87, 5.03, 3.27, 7.29, 8.4,	6.12};

		Print("v1", v1);
		Print("v2", v2);

		sort(v1.begin(), v1.end());

		Print("v1", v1);
	}

	/// @subsubsection stable_sort
	{
		puts("-------------------------------------------------- stable_sort");
		// prime numbers < 100
		std::vector<int> v1 = {83, 53, 47, 23, 13, 59, 29, 41, 19,
							   71, 31, 67, 11, 2,  97, 7,  61, 73,
							   3,  79, 37, 43, 17, 5,  89};
		std::vector<double> v2 = {3.07, 2.49, 3.73, 6.58, 3.3,	2.72, 3.44,
								  8.78, 9.23, 4.09, 4.4,  1.65, 4.92, 0.42,
								  4.87, 5.03, 3.27, 7.29, 8.4,	6.12};

		Print("v1", v1);
		Print("v2", v2);

		std::vector<double> v3;
		Print("v3", v3);

		v3 = v2;
		sort(v3.begin(), v3.end(), mycomp<double>);
		Print("v3", v3);

		v3 = v2;
		stable_sort(v3.begin(), v3.end(), mycomp<double>);
		Print("v3", v3);
	}

	/// -----------------------------------------------------------------------
	/// @subsection Binary search operations (on sorted ranges)
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	puts("Binary search operations (on sorted ranges)");

	/// @subsubsection binary_search
	{
		puts("------------------------------------------------ binary_search");
		int n = 42; // 47
		// prime numbers < 100, out of order
		std::vector<int> v1 = {71, 13, 59, 7,  53, 29, 3,  97, 5,
							   11, 17, 19, 23, 2,  31, 83, 37, 41,
							   89, 43, 47, 61, 67, 73, 79};
		Print("v1", v1);

		sort(v1.begin(), v1.end());
		Print("v1", v1);
		if (binary_search(v1.begin(), v1.end(), n)) {
			std::cout << "found " << n << std::endl;
		} else {
			std::cout << "did not find " << n << std::endl;
		}
	}

	/// -----------------------------------------------------------------------
	/// @subsection Other operations on sorted ranges
	/// -----------------------------------------------------------------------
	puts("==================================================================");
	puts("Other operations on sorted ranges");

	/// @subsubsection merge
	{
		puts("-------------------------------------------------------- merge");

		std::vector<int> v1 = {83, 53, 47, 23, 13, 59, 29, 41, 19, 71, 31, 67};
		std::vector<int> v2 = {2, 97, 7, 61, 73, 3, 79, 37, 43, 17, 5, 89, 11};
		std::vector<int> v3(v1.size() + v2.size());
		Print("v1", v1);
		Print("v2", v2);
		std::cout << std::endl;

		sort(v1.begin(), v1.end());
		sort(v2.begin(), v2.end());
		Print("v1", v1);
		Print("v2", v2);
		std::cout << std::endl;

		merge(v1.begin(), v1.end(), v2.begin(), v2.end(), v3.begin());
		Print("v3", v3);
	}

	return EXIT_SUCCESS;
}
