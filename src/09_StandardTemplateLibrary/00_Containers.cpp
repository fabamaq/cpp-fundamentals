#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Helper Functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Template Operator<<
/// ---------------------------------------------------------------------------

/// @subsubsection vector specialization
template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection forward_list specialization
template <class T>
std::ostream &operator<<(std::ostream &os, const std::forward_list<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection list specialization
template <class T>
std::ostream &operator<<(std::ostream &os, const std::list<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection array specialization
template <class T, std::size_t N>
std::ostream &operator<<(std::ostream &os, const std::array<T, N> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection deque specialization
template <class T>
std::ostream &operator<<(std::ostream &os, const std::deque<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection set specialization
template <class T>
std::ostream &operator<<(std::ostream &os, const std::set<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection map specialization
template <class T, class U>
std::ostream &operator<<(std::ostream &os, const std::map<T, U> &data) {
	for (const std::pair<T, U> &entry : data) {
		os << '{' << entry.first << ':' << entry.second << '}' << ' ';
	}
	return os;
}

/// ---------------------------------------------------------------------------
/// @subsection Template Print Functions
/// ---------------------------------------------------------------------------

/// @subsubsection vector specialization
template <class T>
void Print(const std::string name, const std::vector<T> &data) {
	std::cout << name << ": [ " << data << "]\t"
			  << "size: " << data.size() << "\t"
			  << "capacity: " << data.capacity() << std::endl;
}

/// @subsubsection forward_list specialization
template <class T>
void Print(const std::string name, const std::forward_list<T> &data) {
	std::cout << name << ": [ " << data
			  << "]\t"
			  //   << "size: " << data.size() << "\t"
			  << "max_size: " << data.max_size() << std::endl;
}

/// @subsubsection list specialization
template <class T>
void Print(const std::string name, const std::list<T> &data) {
	std::cout << name << ": [ " << data << "]\t"
			  << "size: " << data.size() << "\t"
			  << "max_size: " << data.max_size() << std::endl;
}

/// @subsubsection array specialization
template <class T, std::size_t N>
void Print(const std::string name, const std::array<T, N> &data) {
	std::cout << name << ": [ " << data << "]\t"
			  << "size: " << data.size() << "\t"
			  << "max_size: " << data.max_size() << std::endl;
}

/// @subsubsection deque specialization
template <class T>
void Print(const std::string name, const std::deque<T> &data) {
	std::cout << name << ": [ " << data << "]\t"
			  << "size: " << data.size() << "\t"
			  << "max_size: " << data.max_size() << std::endl;
}

/// @subsubsection queue specialization
template <class T> void Print(const std::string name, std::queue<T> data) {
	std::deque<T> source;
	while (!data.empty()) {
		source.push_back(data.front());
		data.pop();
	}

	std::cout << name << ": [ ";
	for (const auto &value : source) {
		std::cout << value << ' ';
	}
	std::cout << "]\t"
			  << "size: " << source.size() << std::endl;
}

/// @subsubsection stack specialization
template <class T> void Print(const std::string name, std::stack<T> data) {
	std::deque<T> source;
	while (!data.empty()) {
		source.push_back(data.top());
		data.pop();
	}

	std::cout << name << ": [ ";
	for (const auto &value : source) {
		std::cout << value << ' ';
	}
	std::cout << "]\t"
			  << "size: " << source.size() << std::endl;
}

/// @subsubsection set specialization
template <class T> void Print(const std::string name, std::set<T> data) {
	std::cout << name << ": [ " << data << "]\t"
			  << "size: " << data.size() << "\t"
			  << "max_size: " << data.max_size() << std::endl;
}

/// @subsubsection map specialization
template <class T, class U>
void Print(const std::string name, std::map<T, U> data) {
	std::cout << name << ": [ " << data << "]\t"
			  << "size: " << data.size() << "\t"
			  << "max_size: " << data.max_size() << std::endl;
}

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section STL Containers
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection <vector> - dynamic array
	/// @sa https://en.cppreference.com/w/cpp/container/vector
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("-------------------------------------------------------- vector");
		std::vector<int> numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		Print("numbers", numbers);
		std::cout << "first element: " << numbers.front() << '\n';
		std::cout << "last element:  " << numbers.back() << '\n';
		std::cout << "mid element:   " << numbers.at(numbers.size() / 2)
				  << '\n';
		std::cout << "size: " << numbers.size() << '\n';
		std::cout << "capacity: " << numbers.capacity() << '\n';
		numbers.push_back(12);
		Print("numbers", numbers);
		numbers.emplace_back(13);
		Print("numbers", numbers);
		numbers.clear();
		Print("numbers", numbers);
		numbers.shrink_to_fit();
		Print("numbers", numbers);
		numbers.reserve(100);
		Print("numbers", numbers);
		numbers.resize(32);
		numbers.assign(numbers.size() - 1, 1);
		Print("numbers", numbers);
		numbers.clear();
	}

	/// -----------------------------------------------------------------------
	/// @subsection <forward_list> - singly-linked-list
	/// @sa https://en.cppreference.com/w/cpp/container/forward_list
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("-------------------------------------------------- forward_list");
		std::forward_list<int> numbers{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		Print("numbers", numbers);
		std::cout << "first element: " << numbers.front() << '\n';
		// can't access last element without iteration logic
		auto size = 0U;
		auto iter = numbers.begin();
		auto last = numbers.begin();
		while (++iter != numbers.end()) {
			last++;
			size++;
		}
		std::cout << "last element:  " << *last << '\n';
		// can't access mid element without iteration logic
		size = size / 2;
		auto mid = numbers.begin();
		while (size-- > 0) {
			mid++;
		}
		std::cout << "mid element:  " << *mid << '\n';
		puts("push_front");
		numbers.push_front(0);
		Print("numbers", numbers);
		puts("pop_front");
		numbers.pop_front();
		Print("numbers", numbers);
		puts("push_front");
		numbers.push_front(9);
		Print("numbers", numbers);
		puts("sort");
		numbers.sort();
		Print("numbers", numbers);
		puts("unique");
		numbers.unique();
		Print("numbers", numbers);
	}

	/// -----------------------------------------------------------------------
	/// @subsection <list> - doubly-linked-list
	/// @sa https://en.cppreference.com/w/cpp/container/list
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("---------------------------------------------------------- list");
		std::list<int> numbers{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		Print("numbers", numbers);
		std::cout << "first element: " << numbers.front() << '\n';
		std::cout << "last element:  " << numbers.back() << '\n';
		auto mid = numbers.size() / 2;
		auto iter = numbers.begin();
		while (mid-- > 0) {
			iter++;
		}
		std::cout << "mid element:   " << *iter << '\n';
		numbers.push_front(0);
		Print("numbers", numbers);
		numbers.push_back(12);
		Print("numbers", numbers);
		numbers.pop_front();
		Print("numbers", numbers);
		numbers.pop_back();
		Print("numbers", numbers);
	}

	/// -----------------------------------------------------------------------
	/// @subsection <array>
	/// @sa https://en.cppreference.com/w/cpp/container/array
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("--------------------------------------------------------- array");
		std::array<int, 11> numbers{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		Print("numbers", numbers);
		std::cout << "first element: " << numbers.front() << '\n';
		std::cout << "last element:  " << numbers.back() << '\n';
		std::cout << "mid element:   " << numbers.at(numbers.size() / 2)
				  << '\n';
	}

	/// -----------------------------------------------------------------------
	/// @subsection <deque>
	/// @sa https://en.cppreference.com/w/cpp/container/deque
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("------------------------------------------------------- dequeue");
		std::deque<int> numbers{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		Print("numbers", numbers);
		std::cout << "first element: " << numbers.front() << '\n';
		std::cout << "last element:  " << numbers.back() << '\n';
		std::cout << "mid element:   " << numbers.at(numbers.size() / 2)
				  << '\n';
		numbers.push_front(0);
		Print("numbers", numbers);
		numbers.push_back(12);
		Print("numbers", numbers);
		numbers.emplace_back(13);
		Print("numbers", numbers);
		numbers.clear();
		Print("numbers", numbers);
		numbers.shrink_to_fit();
		Print("numbers", numbers);
		numbers.resize(32);
		numbers.assign(numbers.size() - 1, 1);
		Print("numbers", numbers);
		numbers.clear();
	}

	/// -----------------------------------------------------------------------
	/// @subsection <queue>
	/// @sa https://en.cppreference.com/w/cpp/container/queue
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("--------------------------------------------------------- queue");
		std::deque<int> source = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		std::queue<int> numbers{source};
		Print("numbers", numbers);
		numbers.push(12);
		Print("numbers", numbers);
		numbers.pop();
		Print("numbers", numbers);
	}

	/// -----------------------------------------------------------------------
	/// @subsection <stack>
	/// @sa https://en.cppreference.com/w/cpp/container/stack
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("--------------------------------------------------------- stack");
		std::deque<int> source = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
		std::stack<int> numbers{source};
		Print("numbers", numbers);
		numbers.push(12);
		Print("numbers", numbers);
		numbers.pop();
		Print("numbers", numbers);
	}

	/// -----------------------------------------------------------------------
	/// @subsection <set>
	/// @sa https://en.cppreference.com/w/cpp/container/set
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("----------------------------------------------------------- set");
		std::set<int> numbers({1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11});
		Print("numbers", numbers);
		numbers.insert({11, 12, 13});
		Print("numbers", numbers);
		// contains (c++20)
		if (numbers.count(12) > 0) {
			std::cout << "numbers contains 12\n";
		} else {
			std::cout << "numbers does not contain 12\n";
		}
		numbers.erase(13);
		Print("numbers", numbers);
		if (numbers.count(13) > 0) {
			std::cout << "numbers contains 13\n";
		} else {
			std::cout << "numbers does not contain 13\n";
		}
		numbers.clear();
		/// @note set does not allow duplicates
		numbers.insert({1, 1, 2, 3, 4, 4, 5, 6, 2, 8, 9, 10, 1, 7, 7, 7, 7});
		for (auto number : {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
			assert(numbers.count(number) > 0);
		}

		std::cout << "lower_bound: " << *numbers.lower_bound(7) << '\n';
		std::cout << "upper_bound: " << *numbers.upper_bound(7) << '\n';
	}

	/// -----------------------------------------------------------------------
	/// @subsection <map>
	/// @sa https://en.cppreference.com/w/cpp/container/map
	/// -----------------------------------------------------------------------

	{
		puts("---------------------------------------------------------------");
		puts("----------------------------------------------------------- map");
		std::map<int, int> num_cnt_map; // numbers->count map
		for (int i = 1; i <= 11; i++) {
			num_cnt_map[i] = 0;
		}
		Print("num_cnt_map", num_cnt_map);
		for (auto number : {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) {
			num_cnt_map[number]++;
		}
		Print("num_cnt_map", num_cnt_map);
		std::cout << "lower_bound: " << num_cnt_map.lower_bound(7)->first
				  << '\n';
		std::cout << "upper_bound: " << num_cnt_map.upper_bound(7)->first
				  << '\n';
	}

	return EXIT_SUCCESS;
}
