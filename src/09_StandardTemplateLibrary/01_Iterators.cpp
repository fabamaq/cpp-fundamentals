#include "StandardLibrary.hpp"

void message(const char *m) { std::cout << m << std::endl; }
void message(const char *m, int i) { std::cout << m << ": " << i << std::endl; }

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section STL Iterators
	/// @sa https://en.cppreference.com/w/cpp/iterator
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection Legacy Input Iterator
	/// @sa https://en.cppreference.com/w/cpp/named_req/InputIterator
	/// -----------------------------------------------------------------------

	{
		double d1 = 0, d2 = 0;

		std::cout << "Two numeric values: " << std::flush;
		std::cin.clear();
		std::istream_iterator<double>
			eos; // default constructor is end-of-stream
		std::istream_iterator<double> iit(std::cin); // stdin iterator

		if (iit == eos) {
			puts("no values");
			return 0;
		} else {
			d1 = *iit++;
		}

		if (iit == eos) {
			puts("no second value");
			return 0;
		} else {
			d2 = *iit;
		}

		std::cin.clear();
		std::cout << d1 << " * " << d2 << " = " << d1 * d2 << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Legacy Output Iterator
	/// @sa https://en.cppreference.com/w/cpp/named_req/OutputIterator
	/// -----------------------------------------------------------------------

	{
		std::ostream_iterator<int> output(std::cout, " ");

		for (int i : {3, 197, 42}) {
			*output++ = i;
		}
		std::cout << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Legacy Forward Iterator
	/// @sa https://en.cppreference.com/w/cpp/named_req/ForwardIterator
	/// -----------------------------------------------------------------------

	{
		std::forward_list<int> fl1 = {1, 2, 3, 4, 5};
		std::forward_list<int>::iterator it1; // forward iterator

		// for (it1 = fl1.begin(); it1 != fl1.end(); ++it1) {
		// 	std::cout << *it1 << " ";
		// }

		// uses forward iterator
		for (int i : fl1) {
			std::cout << i << " ";
		}

		std::cout << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Legacy Bidirectional Iterator
	/// @sa https://en.cppreference.com/w/cpp/named_req/BidirectionalIterator
	/// -----------------------------------------------------------------------

	{
		std::set<int> set1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		std::set<int>::iterator it1; // iterator object

		// iterate forward
		for (it1 = set1.begin(); it1 != set1.end(); ++it1) {
			std::cout << *it1 << " ";
		}
		std::cout << std::endl;

		// iterate backward
		for (it1 = set1.end(); it1 != set1.begin();) {
			/// @note we decrement in the body because *.begin() returns an
			/// ... iterator to the beginning instead of 1 past the beginning
			std::cout << *--it1 << " ";
		}
		std::cout << std::endl;

		// range-based for loop
		for (int i : set1) {
			std::cout << i << " ";
		}
		std::cout << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Legacy Random Access Iterator
	/// @sa https://en.cppreference.com/w/cpp/named_req/RandomAccessIterator
	/// -----------------------------------------------------------------------

	{
		std::vector<int> v1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		std::vector<int>::iterator it1; // iterator object

		// iterate forward
		for (it1 = v1.begin(); it1 != v1.end(); ++it1) {
			std::cout << *it1 << " ";
		}
		std::cout << std::endl;

		// iterate backward
		for (it1 = v1.end(); it1 != v1.begin();) {
			/// @note we decrement in the body because *.begin() returns an
			/// ... iterator to the beginning instead of 1 past the beginning
			std::cout << *--it1 << " ";
		}
		std::cout << std::endl;

		// range-based for loop
		for (int i : v1) {
			std::cout << i << " ";
		}
		std::cout << std::endl;

		it1 = v1.begin() + 5;
		message("element begin + 5", *it1);
		message("element [5]", v1[5]);

		it1 = v1.end() - 3;
		message("element end - 3", *it1);
	}

	std::cout << std::endl;

	return EXIT_SUCCESS;
}
