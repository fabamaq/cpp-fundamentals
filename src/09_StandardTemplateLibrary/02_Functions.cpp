#include "StandardLibrary.hpp"

/// ===========================================================================
/// @section Helper Functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Template Operator<<
/// ---------------------------------------------------------------------------

/// @subsubsection partial specialization for vector<T>
template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &data) {
	for (const auto &value : data) {
		os << value << ' ';
	}
	return os;
}

/// @subsubsection full specialization for vector<bool>
std::ostream &operator<<(std::ostream &os, const std::vector<bool> &data) {
	for (const auto &value : data) {
		os << (value ? "T" : "F") << ' ';
	}
	return os;
}

/// ---------------------------------------------------------------------------
/// @subsection Template Print Functions
/// ---------------------------------------------------------------------------

/// @subsubsection vector specialization
template <class T>
void Print(const std::string name, const std::vector<T> &data) {
	std::cout << name << ": [ " << data << "]" << std::endl;
}

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================
int main() {

	/// =======================================================================
	/// @section STL Functions
	/// @sa https://en.cppreference.com/w/cpp/header/functional
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection Arithmetic Functors
	/// -----------------------------------------------------------------------

	/// @subsubsection std::plus
	/// @sa https://en.cppreference.com/w/cpp/utility/functional/plus

	{
		puts("---------------------------------------------------------------");
		std::vector<long> v1 = {26, 52, 79, 114, 183};
		std::vector<long> v2 = {1, 2, 3, 4, 5};
		std::vector<long> v3(v1.size(), 0);
		Print("v1", v1);
		Print("v2", v2);
		Print("v3", v3);
		std::cout << std::endl;

		/*
			pass std::plus as the functio to apply to all elements in v1 and v2
			writing the result to v3
		*/
		std::plus<long> f;
		transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), f);
		Print("v3", v3);
		std::cout << std::endl;
	}

	/// @subsubsection std::negate
	/// @sa https://en.cppreference.com/w/cpp/utility/functional/negate

	{
		puts("---------------------------------------------------------------");
		std::vector<long> v1 = {26, 52, 79, 114, 183};
		std::vector<long> v3(v1.size(), 0);
		Print("v1", v1);
		Print("v3", v3);
		std::cout << std::endl;

		/*
			pass std::negate as the functio to apply to all elements in v1
			writing the result to v3
		*/
		std::negate<long> f;
		transform(v1.begin(), v1.end(), v3.begin(), f);
		Print("v3", v3);
		std::cout << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Relational Functors
	/// -----------------------------------------------------------------------

	puts("===================================================================");
	{
		puts("---------------------------------------------------------------");
		std::vector<long> v1 = {26, 52, 79, 114, 183};
		std::vector<long> v2 = {52, 2, 72, 114, 5};
		std::vector<bool> v3(v1.size());
		Print("v1", v1);
		Print("v2", v2);
		std::cout << std::endl;

		std::greater<long> f;
		transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), f);
		Print("v3", v3);
		std::cout << std::endl;
	}

	/// -----------------------------------------------------------------------
	/// @subsection Logical Functors
	/// -----------------------------------------------------------------------

	puts("===================================================================");
	{
		puts("---------------------------------------------------------------");
		std::vector<int> v1 = {1, 0, 1, 0, 1, 0, 1, 0};
		std::vector<int> v2 = {1, 1, 1, 1, 0, 0, 0, 0};
		std::vector<int> v3(v1.size(), 0);
		Print("v1", v1);
		Print("v2", v2);
		std::cout << std::endl;

		std::logical_and<int> f;
		transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), f);
		Print("v3", v3);
	}

	return EXIT_SUCCESS;
}
