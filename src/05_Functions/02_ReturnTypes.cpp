#include "StandardLibrary.hpp"

int GetValue() { return 42; }

int &GetValueByRef() {
	static int value = 7;
	return value;
}

int main() {

	/// =======================================================================
	/// @section Functions Return Types
	/// =======================================================================

	auto getValueResult = GetValue();
	printf("getValueResult is: %d\n", getValueResult);

	auto getValueByRefResultCopy = GetValueByRef();
	printf("getValueByRefResultCopy is: %d\n", getValueByRefResultCopy);

	auto &getValueByRefResultRef = GetValueByRef();
	printf("getValueByRefResultRef is: %d\n", getValueByRefResultRef);

	getValueByRefResultRef = GetValue();
	puts("change getValueByRefResultRef to result of GetValue() call");

	auto &newGetValueByRefResultRef = GetValueByRef();
	printf("newGetValueByRefResultRef is: %d\n", newGetValueByRefResultRef);

	return EXIT_SUCCESS;
}
