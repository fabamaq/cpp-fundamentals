#include "StandardLibrary.hpp"

void foo() { puts("::foo()"); }

void callByValue(int byValue) {
	puts("callByValue()");
	byValue++;
}

void callByReference(int &byReference) {
	puts("callByValue()");
	byReference++;
}

void callByPointer(int *byPointer) {
	puts("callByPointer()");
	--(*byPointer);
}

int main() {

	/// =======================================================================
	/// @section Overview
	/// =======================================================================

	foo(); // calls the global function foo

	/// -----------------------------------------------------------------------
	/// @subsection Pass arguments by value makes a copy of the arguments
	/// -----------------------------------------------------------------------

	int i = 7;
	callByValue(i);
	printf("i = %d\n", i);

	/// -----------------------------------------------------------------------
	/// @subsection Pass arguments by reference changes the arguments (no copy)
	/// -----------------------------------------------------------------------

	callByReference(i);
	printf("i = %d\n", i);

	/// -----------------------------------------------------------------------
	/// @subsection Pass pointers by value changes the variable pointed to
	/// -----------------------------------------------------------------------

	callByPointer(&i);
	printf("i = %d\n", i);

	return EXIT_SUCCESS;
}
