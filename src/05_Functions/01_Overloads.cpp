#include "StandardLibrary.hpp"

void f(int value) {
	puts("f(int value);");
	value++;
}

void f(int *value) {
	puts("f(int* value);");
	value++;
}

/// @note if we overload f with (int&), the call is ambiguous, so we use 'g'
void g(int &value) {
	puts("g(int& value)");
	value--;
}

void h(const int &value) {
	puts("h(const int& value)");
	printf("value is: %d\n", value);
	// value++;
}

void h(const int *pointer) {
	puts("h(const int& pointer)");
	printf("pointer is: %p\n", pointer);
	printf("value is: %d\n", *pointer);
	*pointer++;
	pointer = nullptr;
}

void print(int value) { printf("value: %d\n", value); }

void print(const std::vector<int> &data) {
	printf("data: [ ");
	for (auto value : data) {
		printf("%d ", value);
	}
	puts("]");
}

void print(const char *message) { printf("message: \"%s\"\n", message); }

void print(const std::vector<int> &data, const char *name) {
	printf("%s: [ ", name);
	for (auto value : data) {
		printf("%d ", value);
	}
	puts("]");
}

int main() {

	/// =======================================================================
	/// @section Function overloading (use same name for different functions)
	/// =======================================================================

	int x = 7;

	// overloading f
	f(x);
	printf("x: %d\n", x);
	f(&x);
	printf("x: %d\n", x);

	g(x);
	printf("x: %d\n", x);

	// overloading h
	h(x);
	printf("x: %d\n", x);
	h(&x);
	printf("x: %d\n", x);
	printf("&: %p\n", &x);

	print(21);
	print({3, 7});
	print("Hello World");
	print({1, 2, 3, 4, 5}, "numbers");

	return EXIT_SUCCESS;
}
