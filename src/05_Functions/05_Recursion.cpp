#include "StandardLibrary.hpp"

unsigned long int factorial(unsigned long int n) {
	if (n < 2)
		return 1;
	return n * factorial(n - 1);
}

int main() {

	/// =======================================================================
	/// @section Recursive Functions
	/// =======================================================================

	unsigned long int n = 5;
	printf("Factorial of %ld is %ld\n", n, factorial(n));

	return EXIT_SUCCESS;
}
