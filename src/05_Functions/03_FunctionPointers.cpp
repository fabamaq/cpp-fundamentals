#include "StandardLibrary.hpp"

void OldFunction() { puts("OldFunction()"); }

void NewFunction() { puts("NewFunction()"); }

struct Functor {
	void operator()() { puts("Functor::operator()()"); }
};

int main() {

	/// =======================================================================
	/// @section Function Pointers
	/// =======================================================================

	void (*pFunc)() = OldFunction;
	(*pFunc)();

	pFunc = NewFunction;
	(*pFunc)();

	return EXIT_SUCCESS;
}
