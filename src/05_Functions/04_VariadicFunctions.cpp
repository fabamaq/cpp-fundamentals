#include "StandardLibrary.hpp"

/// @note '...' is the variadic parameter
void log(const char *fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	int rc = vfprintf(stdout, fmt, ap);
	puts("");
	va_end(ap);
}

/// @note '...' is the variadic parameter
int sumOf(const int count, ...) {
	va_list ap;
	int sum = 0;

	va_start(ap, count);
	for (int i = 0; i < count; ++i) {
		sum += va_arg(ap, int);
	}
	va_end(ap);
	return sum;
}

int main() {

	/// =======================================================================
	/// @section Variadic Functions
	/// =======================================================================

	log("This is a log");
	log("Sum of numbers: %d", sumOf(6, 1, 2, 3, 4, 5, 6));

	return EXIT_SUCCESS;
}
