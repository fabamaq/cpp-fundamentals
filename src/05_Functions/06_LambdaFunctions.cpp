#include "StandardLibrary.hpp"

int main() {

	/// =======================================================================
	/// @section Lambda Functions (c++11 or higher)
	/// =======================================================================

	const auto lambdaFunction = []() { puts("Running a lambdaFunction"); };
	lambdaFunction();

	/// -----------------------------------------------------------------------
	/// @subsection Captures
	/// -----------------------------------------------------------------------

	/// @subsubsection captures by value

	{
		// defines a function that given an `int` returns a `std::string`
		const auto fizzBuzz = [](int value) -> std::string {
			// `value` is captured by value into the lambda function
			const auto isMultipleOf = [value](int base) -> bool {
				return (0 == (value % base));
			};
			if (isMultipleOf(15)) {
				return "FizzBuzz";
			}
			if (isMultipleOf(5)) {
				return "Buzz";
			}
			if (isMultipleOf(3)) {
				return "Fizz";
			}
			return std::to_string(value);
		};

		// calls fizzBuzz function for every number in the list
		for (int i : {1, 2, 3, 4, 5, 6, 9, 10, 15, 30}) {
			printf("FizzBuzzOf(%d) is %s\n", i, fizzBuzz(i).c_str());
		}
	}

	/// @subsubsection captures by reference

	{
		constexpr size_t _maxlen = 128;
		char lastC = 0;
		char s[] = "big light in sky slated to appear in east";

		// `lastC` is captured by reference into the lambda function
		const auto toTitleCase = [&lastC](const char &c) -> char {
			const char r =
				(lastC == ' ' || lastC == 0) ? toupper(c) : tolower(c);
			lastC = c;
			return r;
		};

		std::transform(s, s + strnlen(s, _maxlen), s, toTitleCase);
		puts(s);
	}

	return EXIT_SUCCESS;
}
