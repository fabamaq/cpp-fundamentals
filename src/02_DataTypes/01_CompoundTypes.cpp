#include "StandardLibrary.hpp"

// myFunction declaration (function that has no parameters and retursn nothing)
void myFunction();

// two functions that have 2 int parameters and return an int
int add(int a, int b);
int sub(int a, int b);

/// @brief Application entry point
int main() {

	/// =======================================================================
	/// @section Compound Types
	/// @sa https://en.cppreference.com/w/cpp/language/type
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection reference types
	/// -----------------------------------------------------------------------
	puts("Reference Types");

	int anIntValue = 7;
	int &anIntReference = anIntValue;

	printf("anIntValue: %d\n", anIntValue);
	printf("anIntReference: %d\n", anIntReference);

	// references can change the value of the original variable
	anIntReference = 3;

	// references cannot be assigned to another variable
	int anotherIntValue = 21;
	// ! this changes `anIntValue`
	anIntReference = anotherIntValue;

	printf("anIntValue: %d\n", anIntValue);
	printf("anIntReference: %d\n", anIntReference);
	printf("anotherIntValue: %d\n", anotherIntValue);

	/// -----------------------------------------------------------------------
	/// @subsection pointer types
	/// @sa 08_Pointers chapter
	/// -----------------------------------------------------------------------
	puts("Pointer types");

	// a pointer to the address of anIntValue (of type int)
	int *aPointerToIntVariable = &anIntValue;
	*aPointerToIntVariable = 7; // change the value at the address of anIntValue

	printf("anIntValue: %d\n", anIntValue);
	printf("anIntReference: %d\n", anIntReference);
	printf("anotherIntValue: %d\n", anotherIntValue);

	// nullptr_t
	void *pointer = nullptr; // pointer to void initialized to "null"

	/// -----------------------------------------------------------------------
	/// @subsection array types
	/// -----------------------------------------------------------------------

	// an int array of size 3 (size is defined by the programmer)
	int anIntArray[3] = {1, 2, 3};
	printf("anIntArray: [ ");
	for (int i = 0; i < 3; i++) {
		printf("%d ", anIntArray[i]);
	}
	puts("]");

	// another int array of size 3 (size is defined by the compiler)
	int anotherIntArray[] = {4, 5, 6};

	int aMultidimensionalArray[3][3] = {
		{1, 0, 0},
		{0, 1, 0},
		{0, 0, 1},
	};

	puts("anIntMatrix(3x3): [");
	for (int row = 0; row < 3; row++) {
		printf("  [ ");
		for (int col = 0; col < 3; col++) {
			printf("%d ", aMultidimensionalArray[row][col]);
		}
		puts("]");
	}
	puts("]");

	/// -----------------------------------------------------------------------
	/// @subsection function types
	/// -----------------------------------------------------------------------

	// call a void function that returns nothing
	myFunction();

	int sumOfArrays = 0;
	for (int i = 0; i < 3; i++) {
		// calls the add function that returns the sum of two values ...
		// and accumulates that result in sumOfArrays (addition in two ways)
		sumOfArrays += add(anIntArray[i], anotherIntArray[i]);
	}
	printf("The sum of anIntArray and anotherIntArray is %d\n", sumOfArrays);
	printf("Take the most powerful number, we get: %d\n", sub(sumOfArrays, 7));

	/// -----------------------------------------------------------------------
	/// @subsection enumeration types
	/// -----------------------------------------------------------------------

	// unscoped enum
	enum UnscopedColor { red, green, blue };
	puts("Choose a color:");
	puts("0 - red");
	puts("1 - green");
	puts("2 - blue");

	UnscopedColor anUnscopedColor;
	int c;
	scanf("%d", &c);

	anUnscopedColor = static_cast<UnscopedColor>(c);
	switch (anUnscopedColor) {
	case red:
		puts("red");
		break;
	case green:
		puts("green");
		break;
	default:
		puts("blue"); // only one left right? WRONG! 2, 3, etc. are all accepted
		break;
	}

	enum class ScopedColor { Red, Green, Blue };
	puts("Choose a color:");
	puts("0 - red");
	puts("1 - green");
	puts("2 - blue");

	ScopedColor aScopedColor;
	scanf("%d", &c);

	aScopedColor = static_cast<ScopedColor>(c);
	switch (aScopedColor) {
	case ScopedColor::Red:
		puts("Red");
		break;
	case ScopedColor::Green:
		puts("Green");
		break;
	case ScopedColor::Blue:
		puts("Blue");
		break;
	default:
		puts("Wrong color");
		break;
	}

	/// -----------------------------------------------------------------------
	/// @subsection class types
	/// @sa ClassesAndObjects
	/// -----------------------------------------------------------------------

	/*
		MyStruct is a struct
		everything in a struct is 'public' unless specified otherwise
	*/
	struct MyStruct {
		int value;

	  private:
		int secret;
	};

	MyStruct myStruct;
	myStruct.value = 21;
	printf("myStruct value is %d\n", myStruct.value);

	/*
		MyClass is a class
		everything in a class is 'private' unless specified otherwise
	*/
	class MyClass {
		int value{0};
		/*
			everything in the private section is inaccessible outside the class
			but it is accessible inside the class
		*/
	  public:
		/*
		  everything in the public section is accessible outside the class
		*/
		int getValue() { return value; }
		void setValue(int newValue) { value = newValue; }
		void print() { printf("my value is %d\n", value); }
	};

	MyClass myClass;
	printf("myClass value is %d\n", myClass.getValue());
	myClass.setValue(21);
	printf("myClass value is %d\n", myClass.getValue());
	myClass.print();

	/// -----------------------------------------------------------------------
	/// @subsection union types
	/// @sa https://en.cppreference.com/w/cpp/language/union
	/// -----------------------------------------------------------------------

	union Vec3 {
		struct {
			int x, y, z;
		} position;
		struct {
			int r, g, b;
		} color;
	};

	Vec3 aColorVec3;
	aColorVec3.color.r = 255;
	aColorVec3.color.g = 255;
	aColorVec3.color.b = 255;

	Vec3 aPosition;
	aPosition.position.x = 0;
	aPosition.position.y = 0;
	aPosition.position.z = 0;

	/// -----------------------------------------------------------------------
	/// @subsection Bit Fields
	/// -----------------------------------------------------------------------

	struct OldParameters {
		bool GenerateSingleCard;
		bool ClientCanSetCards;
		bool HasWildBall;
		bool IsMathDebug;
		bool hasMegaSales;
		bool hasMegaDiscount;
		bool hasMegaBonus;
		bool hasMegaBingo;
	};

	static_assert(sizeof(OldParameters) == 8, "sizeof(OldParameters)");

	struct NewParameters {
		bool GenerateSingleCard : 1;
		bool ClientCanSetCards : 1;
		bool HasWildBall : 1;
		bool IsMathDebug : 1;
		bool hasMegaSales : 1;
		bool hasMegaDiscount : 1;
		bool hasMegaBonus : 1;
		bool hasMegaBingo : 1;
	};

	static_assert(sizeof(NewParameters) == 1, "sizeof(NewParameters)");

	return EXIT_SUCCESS;
}

// myFunction definition
void myFunction() { puts("myFunction was called"); }

int add(int a, int b) { return a + b; }
int sub(int a, int b) { return a - b; }
