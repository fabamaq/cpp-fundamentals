#include "StandardLibrary.hpp"

std::vector<int> GetVector() {
	// static local variable defined once and remains until main exits
	static std::vector<int> result;
	if (result.empty()) {
		result.reserve(42);
		result.resize(42);
		std::iota(result.begin(), result.end(), 1);
	}
	return result;
}

/// @brief Application entry point
int main() {

	/// =======================================================================
	/// @section <cstdint>
	/// =======================================================================

	printf("sizeof int8_t is %ld bits\n", sizeof(int8_t) * 8);
	printf("sizeof int16_t is %ld bits\n", sizeof(int16_t) * 8);
	printf("sizeof int32_t is %ld bits\n", sizeof(int32_t) * 8);
	printf("sizeof int64_t is %ld bits\n", sizeof(int64_t) * 8);

	printf("sizeof uint8_t is %ld bits\n", sizeof(uint8_t) * 8);
	printf("sizeof uint16_t is %ld bits\n", sizeof(uint16_t) * 8);
	printf("sizeof uint32_t is %ld bits\n", sizeof(uint32_t) * 8);
	printf("sizeof uint64_t is %ld bits\n", sizeof(uint64_t) * 8);

	// printf("sizeof char8_t is %ld bits\n", sizeof(char8_t) * 8); // c++20
	printf("sizeof char16_t is %ld bits\n", sizeof(char16_t) * 8);
	printf("sizeof char32_t is %ld bits\n", sizeof(char32_t) * 8);

	/// =======================================================================
	/// @section Iterators (more commonly used with algorithms and the STL)
	/// =======================================================================
	std::vector<int> numbers = {1, 2, 3, 4, 5};

	std::vector<int>::const_iterator iter = numbers.cbegin();
	printf("numbers: [ ");
	for (; iter != numbers.cend(); iter++) {
		printf("%d ", *iter);
	}
	puts("]");

	/// =======================================================================
	/// @section Auto
	/// =======================================================================

	// automatically deduces the type of GetVector()'s return type
	auto newNumbers = GetVector();

	// automatically deduces the type of newNumbers.cbegin()
	auto anotherIter = newNumbers.cbegin();
	printf("newNumbers: [ ");
	for (; anotherIter != newNumbers.cend(); anotherIter++) {
		printf("%d ", *anotherIter);
	}
	puts("]");

	auto anInt = 21;
	auto anUnsignedInt = 42U;

	return EXIT_SUCCESS;
}
