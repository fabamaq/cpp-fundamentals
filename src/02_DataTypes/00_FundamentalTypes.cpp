/******************************************************************************
 * @copyright FABAMAQ 2021. All Rights Reserved
 *****************************************************************************/
#include "StandardLibrary.hpp"

/// @brief Application entry point
int main() {

	/// =======================================================================
	/// @section Fundamental Types
	/// @sa https://en.cppreference.com/w/cpp/language/type
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection Void type
	/// -----------------------------------------------------------------------
	// void voidVariable; // an incomplete type, therefore, can't define a var

	/// -----------------------------------------------------------------------
	/// @subsection integer types
	/// -----------------------------------------------------------------------

	/// unsigned
	unsigned short anUnsignedShort; // a number between 0 and 65,535
	unsigned int anUnsignedInt;		// a number between 0 and 4,294,967,295
	unsigned long anUnsignedLong;	// it depends on the architecture (32 vs 64)
	unsigned long long
		anUnsignedLongLong; // a number between 0 and 18,446,744,073,709,551,615

	static_assert(sizeof(anUnsignedShort) == 2, "sizeof(unsigned short) is 2");
	static_assert(sizeof(anUnsignedInt) == 4, "sizeof(unsigned int) is 4");
	/// @note sizeof(unsigned long) depends on the architecture
	static_assert(sizeof(anUnsignedLongLong) == 8,
				  "sizeof(unsigned long long) is 8");

	/// signed
	short aShort;		 // a number between -32,768 to 32,767
	int anInt;			 // a number between -2,147,483,648 to 2,147,483,647
	long aLong;			 // it depends on the architecture (32 vs 64)
	long long aLongLong; // a number between -(2^63) to (2^63)-1

	static_assert(sizeof(aShort) == 2, "sizeof(signed short) is 2");
	static_assert(sizeof(anInt) == 4, "sizeof(signed int) is 4");
	/// @note sizeof(long) depends on the architecture
	static_assert(sizeof(aLongLong) == 8, "sizeof(signed long long) is 8");

	/// -----------------------------------------------------------------------
	/// @subsection boolean type
	/// -----------------------------------------------------------------------
	bool aBooleanValue; // can be `true` or `false`

	/// -----------------------------------------------------------------------
	/// @subsection character types
	/// -----------------------------------------------------------------------
	char aChar;					  // a number between -128 and 127
	unsigned char anUnsignedChar; // a number between 0 and 255

	static_assert(sizeof(anUnsignedChar) == 1, "sizeof(unsigned char) is 1");
	static_assert(sizeof(aChar) == 1, "sizeof(signed char) is 1");

	wchar_t aWideChar; // a character type that supports Unicode and UTF-16
	/// @note sizeof(wchar_t) depends on the architecture (can be 2 or 4 bytes)

	/// -----------------------------------------------------------------------
	/// @subsection floating-point
	/// -----------------------------------------------------------------------

	/// floating-point numbers' range is implementation-defined
	float aFloat;			 // a floating-point number with 4 bytes
	double aDouble;			 // a floating-point number with 8 bytes
	long double aLongDouble; // a floating-point number with 12 bytes

	puts("Press any key to continue...");
	int c;
	c = getchar();
	c = getchar();

	return EXIT_SUCCESS;
}
