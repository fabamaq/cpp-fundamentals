/// @sa https://www.youtube.com/watch?v=ZusiKXcz_ac

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <algorithm>
#include <bitset>
#include <chrono>
#include <numeric>
#include <random>
#include <vector>

/// ===========================================================================
/// @section Helper Functions
/// ===========================================================================

constexpr std::size_t cxMaxSize = 100000000; // 10M

inline std::vector<long> GetDataA() {
	static std::vector<long> result;
	if (result.empty()) {
		std::default_random_engine dre;
		result.resize(cxMaxSize);
		std::iota(result.begin(), result.end(), 1);
		std::shuffle(result.begin(), result.end(), dre);
	}
	return result; // copy
}

inline std::vector<long> GetDataB() {
	static std::vector<long> result;
	if (result.empty()) {
		std::default_random_engine dre;
		result.resize(cxMaxSize);
		std::iota(result.begin(), result.end(), 1);
		std::shuffle(result.begin(), result.end(), dre);
	}
	return result; // copy
}

/// ===========================================================================
/// @section Operators
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection Bitwise Operators
/// ---------------------------------------------------------------------------

constexpr unsigned char A = 0b10110011;
constexpr unsigned char B = 0b01101001;

static_assert((A & B) == 0b00100001, "& failed");
static_assert((A | B) == 0b11111011, "| failed");
static_assert((A ^ B) == 0b11011010, "^ failed");

static_assert(static_cast<unsigned char>((~A)) == 0b01001100, "~ failed");
static_assert((A >> 3) == 0b00010110, ">> failed");
static_assert(static_cast<unsigned char>((A << 2)) == 0b11001100, "<< failed");

/// @brief Sets the Kth Bit in a Word
#define SET_BIT(x, k) x | (1 << k)

void testSetTheKthBit() {
	puts("testSetTheKthBit");
	std::bitset<16> x(0b1011110101101101);
	printf("%s\n", x.to_string().c_str());
	constexpr unsigned int k = 7;
	std::bitset<16> y(SET_BIT(x.to_ulong(), 7));
	printf("%s\n", y.to_string().c_str());
}

/// @brief Clears the Kth Bit in a Word
#define CLEAR_BIT(x, k) x & ~(1 << k)

void testClearTheKthBit() {
	puts("testClearTheKthBit");
	std::bitset<16> x(0b1011110111101101);
	printf("%s\n", x.to_string().c_str());
	constexpr unsigned int k = 7;
	std::bitset<16> y(CLEAR_BIT(x.to_ulong(), k));
	printf("%s\n", y.to_string().c_str());
}

/// @brief Toggle the Kth Bit in a Word
#define TOGGLE_BIT(x, k) x ^ (1 << k)

void testToggleTheKthBit() {
	puts("testToggleTheKthBit");
	std::bitset<16> x(0b1011110111101101);
	printf("%s\n", x.to_string().c_str());
	constexpr unsigned int k = 7;
	std::bitset<16> y(TOGGLE_BIT(x.to_ulong(), k));
	printf("%s\n", y.to_string().c_str());
	std::bitset<16> z(TOGGLE_BIT(y.to_ulong(), k));
	printf("%s\n", z.to_string().c_str());
}

/// @brief Extract a Bit Field
#define EXTRACT_BIT_FIELD(x, mask, shift) (x & mask) >> shift

void testExtractBitField() {
	puts("testExtractBitField");
	std::bitset<16> x(0b1011110111101101);
	printf("%s\n", x.to_string().c_str());
	std::bitset<16> mask(0b0000011110000000);
	printf("%s\n", mask.to_string().c_str());
	constexpr unsigned int shift = 7;
	auto result = EXTRACT_BIT_FIELD(x.to_ulong(), mask.to_ulong(), shift);
	std::bitset<16> z(result);
	printf("%s\n", z.to_string().c_str());
}

/// @brief Set a Bit Field
#define SET_BIT_FIELD(x, mask, y, shift) (x & ~mask) | ((y << shift) & mask)

void testSetBitField() {
	puts("testSetBitField");
	std::bitset<16> x(0b1011110111101101);
	printf("%s\n", x.to_string().c_str());

	std::bitset<16> y(0b0000000000000011);
	printf("%s\n", y.to_string().c_str());

	std::bitset<16> mask(0b0000011110000000);
	printf("%s\n", mask.to_string().c_str());

	constexpr unsigned int shift = 7;
	auto result = EXTRACT_BIT_FIELD(x.to_ulong(), mask.to_ulong(), shift);
	std::bitset<16> z(result);
	printf("%s\n", z.to_string().c_str());
}

/// @brief Ordinary Swap
void testNoTempSwap() {
	unsigned int x = 0b10111101;
	unsigned int y = 0b00101110;
	printf("x: %d\n", x);
	printf("y: %d\n", y);

	// ! Poor at exploiting instruction-level parallelism (ILP)
	x = x ^ y; // Mask with 1's where bits differ
	y = x ^ y; // Flip bits in y that differ from x
	x = x ^ y; // Flip bits in x that differ from y

	printf("x: %d\n", x);
	printf("y: %d\n", y);
}

void testFindMinimumOfTwoIntegers() {
	int x = 3;
	int y = 7;
	int r;

	/// @brief if-else approach
	// if (x < y)
	// 	r = x;
	// else
	// 	r = y;

	/// @note A mispredicted branch empties the processor pipeline
	/// @brief ternary op approach
	// r = (x < y) ? x : y;

	/// @brief bit tricks approach
	// Find the minimum r of two integers x and y without using a branch
	r = y ^ ((x ^ y) & ~(x < y));

	printf("r: %d\n", r);
}

/// @brief Merging Two Sorted Arrays
/// @note `__restrict` tells the compiler that will be only 1 pointer to that
/// address which allows the compiler to optimize even further
static void merge(long *__restrict C, long *__restrict A, long *__restrict B,
				  std::size_t na, std::size_t nb) {

	/// @note this branch is predictable
	while (na > 0 && nb > 0) {
		/// @note this branch is NOT predictable
		if (*A <= *B) {
			*C++ = *A++;
			na--;
		} else {
			*C++ = *B++;
			nb--;
		}
	}

	/// @note this branch is predictable
	while (na > 0) {
		*C++ = *A++;
		na--;
	}

	/// @note this branch is predictable
	while (nb > 0) {
		*C++ = *B++;
		nb--;
	}
}

void testMerge() {
	long A[]{3, 12, 19, 46};
	long B[]{4, 14, 21, 23};
	constexpr auto na = sizeof(A) / sizeof(long);
	constexpr auto nb = sizeof(B) / sizeof(long);
	long C[na + nb];
	merge(C, A, B, na, nb);
	printf("C: [ ");
	for (long i = 0; i < na + nb; i++) {
		printf("%ld ", C[i]);
	}
	puts("]");
	// prints [ 3 4 12 14 19 21 23 46 ]
}

/// @brief Merging Two Sorted Arrays
/// @note `__restrict` tells the compiler that will be only 1 pointer to that
/// address which allows the compiler to optimize even further
static void merge_branchless(long *__restrict C, long *__restrict A,
							 long *__restrict B, std::size_t na,
							 std::size_t nb) {

	/// @note this branch is predictable
	while (na > 0 && nb > 0) {
		/// @note this branch is predictable
		long cmp = (*A <= *B);
		long min = *B ^ ((*B ^ *A) & (~cmp));
		*C++ = min;
		A += cmp;
		na -= cmp;
		B += !cmp;
		nb -= !cmp;
	}

	/// @note this branch is predictable
	while (na > 0) {
		*C++ = *A++;
		na--;
	}

	/// @note this branch is predictable
	while (nb > 0) {
		*C++ = *B++;
		nb--;
	}
}

void testMergeBranchless() {
	long A[]{3, 12, 19, 46};
	long B[]{4, 14, 21, 23};
	constexpr auto na = sizeof(A) / sizeof(long);
	constexpr auto nb = sizeof(B) / sizeof(long);
	long C[na + nb];
	merge_branchless(C, A, B, na, nb);
	printf("C: [ ");
	for (long i = 0; i < na + nb; i++) {
		printf("%ld ", C[i]);
	}
	puts("]");
	// prints [ 3 4 12 14 19 21 23 46 ]
}

void benchmarkMerge() {
	auto A = GetDataA();
	auto B = GetDataB();
	const auto na = A.size();
	const auto nb = B.size();

	std::vector<long> C(na + nb);

	auto start = std::chrono::high_resolution_clock::now();
	merge(C.data(), A.data(), B.data(), na, nb);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	printf("Test took %f seconds\n", diff.count());
}

void benchmarkMergeBranchless() {
	auto A = GetDataA();
	auto B = GetDataB();
	const auto na = A.size();
	const auto nb = B.size();

	std::vector<long> C(na + nb);

	auto start = std::chrono::high_resolution_clock::now();
	merge_branchless(C.data(), A.data(), B.data(), na, nb);
	auto end = std::chrono::high_resolution_clock::now();

	std::chrono::duration<double> diff = end - start;
	printf("Test took %f seconds\n", diff.count());
}

/// @brief Modular Addition (Compute (x + y) mod n)
void testModularAddition() {
	int x = 1023;
	int y = 42;
	int n = 10;
	int r;

	/// @note using division is expensive, unless by a power of 2
	// int r = (x + y) % n;

	/// @note unpredictable branch is expensive
	// auto z = x + y;
	// r = (z < n) ? z : z - n;

	/// @note using the minimum trick
	auto z = x + y;
	r = z - (n & -(z >= n));
}

/// ===========================================================================
/// @brief Application entry point
/// ===========================================================================
int main() {
	testSetTheKthBit();
	testClearTheKthBit();
	testToggleTheKthBit();
	testExtractBitField();
	testSetBitField();
	testNoTempSwap();

	testFindMinimumOfTwoIntegers();

	testMerge();
	testMergeBranchless();
	benchmarkMerge();
	benchmarkMergeBranchless();

	return EXIT_SUCCESS;
}
