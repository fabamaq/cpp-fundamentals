#include "StandardLibrary.hpp"

void simpleFunction() { puts("simpleFunction()"); }

struct Fraction {
	int n;
	int d;

	friend std::ostream &operator<<(std::ostream &os, const Fraction &rhs) {
		if (rhs.n == rhs.d) {
			os << 1;
		} else {
			os << rhs.n << '/' << rhs.d;
		}
		return os;
	}
};

/// -----------------------------------------------------------------------
/// @subsection Binary arithmetic operators
/// -----------------------------------------------------------------------

Fraction operator+(const Fraction &lhs, const Fraction &rhs) {
	Fraction result;
	result.n = (lhs.n * rhs.d) + (rhs.n * lhs.d);
	result.d = lhs.d * rhs.d;
	return result;
}

Fraction operator-(const Fraction &lhs, const Fraction &rhs) {
	Fraction result;
	result.n = (lhs.n * rhs.d) - (rhs.n * lhs.d);
	result.d = lhs.d * rhs.d;
	return result;
}

Fraction operator*(const Fraction &lhs, const Fraction &rhs) {
	Fraction result;
	result.n = lhs.n * rhs.n;
	result.d = lhs.d * rhs.d;
	return result;
}

Fraction operator/(const Fraction &lhs, const Fraction &rhs) {
	Fraction result;
	result.n = lhs.n;
	result.d = lhs.d * rhs.n * rhs.d;
	return result;
}

/// -----------------------------------------------------------------------
/// @subsection Comparison operators
/// -----------------------------------------------------------------------

bool operator==(const Fraction &lhs, const Fraction &rhs) {
	return lhs.n == rhs.n && lhs.d == rhs.d;
}

bool operator!=(const Fraction &lhs, const Fraction &rhs) {
	return !(lhs == rhs);
}

bool operator<(const Fraction &lhs, const Fraction &rhs) {
	int Ln, Rn;
	Ln = lhs.n * rhs.d;
	Rn = rhs.n * lhs.d;
	return Rn < Ln;
}

bool operator>(const Fraction &lhs, const Fraction &rhs) { return rhs < lhs; }

bool operator<=(const Fraction &lhs, const Fraction &rhs) {
	return !(lhs > rhs);
}

bool operator>=(const Fraction &lhs, const Fraction &rhs) {
	return !(lhs < rhs);
}

int main() {

	/// =======================================================================
	/// @section Operators Types
	/// @sa https://en.cppreference.com/w/cpp/language/operators
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection Assignment operators
	/// -----------------------------------------------------------------------
	int a = 7; // ! not assignment operators (initializing is not assignment)
	int b = 3; // ! not assignment operators (initializing is not assignment)

	// copy assignment
	a = b; // copies the value of b into a

	/// -----------------------------------------------------------------------
	/// @subsection Stream extraction and insertion operators
	/// -----------------------------------------------------------------------
	std::cout << "What is your name?\n";
	char input[255];
	std::cin >> input;
	std::cout << "Hello, " << input << '\n';

	/// -----------------------------------------------------------------------
	/// @subsection Function call operator
	/// -----------------------------------------------------------------------

	simpleFunction(); // calls simpleFunction

	struct Functor {
		void operator()() { puts("Functor::operator()"); }
	};
	Functor myFunctor;
	myFunctor(); // calls operator() in Functor struct

	struct Sum {
		int operator()(int a, int b) { return a + b; }
	};
	Sum mySum;
	printf("sum of 3 and 7 is %d\n", mySum(3, 7));

	/// -----------------------------------------------------------------------
	/// @subsection Increment and Decrement operators
	/// -----------------------------------------------------------------------

	// decrement
	int counter = 10;
	printf("counter is %d\n", counter);
	counter--;
	printf("counter is %d after post-decrement\n", counter);
	--counter;
	printf("counter is %d after pre-decrement\n", counter);
	printf("counter still is %d post-decrement\n", counter--);
	printf("but just after is %d\n", counter);
	printf("and with pre-decrement is %d\n", --counter);

	// increment
	while (counter < 10) {
		counter++;
	}
	puts("same applies with increment");

	/// -----------------------------------------------------------------------
	/// @subsection Binary Arithmetic operators
	/// -----------------------------------------------------------------------

	Fraction f1;
	f1.n = 1;
	f1.d = 2;

	Fraction f2;
	f2.n = 5;
	f2.d = 7;

	std::cout << "f1 + f2 = " << (f1 + f2) << '\n';
	std::cout << "f1 - f2 = " << (f1 - f2) << '\n';
	std::cout << "f1 * f2 = " << (f1 * f2) << '\n';
	std::cout << "f1 / f2 = " << (f1 / f2) << '\n';

	/// -----------------------------------------------------------------------
	/// @subsection Comparison operators
	/// -----------------------------------------------------------------------

	Fraction f3;
	f3.n = 1;
	f3.d = 2;

	std::cout << std::boolalpha << "(f1 == f2): " << (f1 == f2) << '\n'
			  << "(f1 == f3): " << (f1 == f3) << '\n'
			  << "(f1 < f2):  " << (f1 < f2) << '\n'
			  << "(f2 < f1):  " << (f2 < f1) << '\n'
			  << "(f1 < f3):  " << (f1 < f3) << '\n'
			  << "(f2 < f3):  " << (f2 < f3) << '\n';

	/// -----------------------------------------------------------------------
	/// @subsection Bitwise Arithmetic operators
	/// -----------------------------------------------------------------------

	unsigned int bitmaskA = 0b10110011;
	unsigned int bitmaskB = 0b01101001;

	// bitwise AND
	assert((bitmaskA & bitmaskB) == 0b00100001);
	puts("0b10110011 & 0b01101001 = 0b00100001 (1 if both are 1s)");

	// bitwise OR
	assert((bitmaskA | bitmaskB) == 0b11111011);
	puts("0b10110011 | 0b01101001 = 0b11111011 (1 if either is 1");

	// bitwise XOR
	assert((bitmaskA ^ bitmaskB) == 0b11011010);
	puts("0b10110011 ^ 0b01101001 = 0b11011010 (1 if different)");

	// bitwise NEGATION
	assert(static_cast<unsigned char>(~bitmaskA) == 0b01001100);
	puts("~0b10110011             = 0b01001100");

	// bitwise SHIFT RIGHT
	assert(static_cast<unsigned char>(bitmaskA >> 3) == 0b00010110);
	puts("(0b10110011 >> 3)       = 0b00010110");

	// bitwise SHIFT LEFT
	assert(static_cast<unsigned char>(bitmaskA << 2) == 0b11001100);
	puts("(0b10110011 << 2)       = 0b11001100");

	return EXIT_SUCCESS;
}
