#include <cstdio>
#include <cstdlib>
#include <string>

constexpr int maxstring = 1024; // read buffer size
constexpr int repeat = 5;

/// ===========================================================================
/// @section Helper functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection returns a string of this file path thanks to the __FILE__ macro
/// ---------------------------------------------------------------------------
std::string get_file(const char *file) { return std::string(file); }
#define GET_THIS_FILE_PATH() get_file(__FILE__)

/// ---------------------------------------------------------------------------
/// @subsection returns the cwd by erasing "02_RAII.cpp" from this file path
/// ---------------------------------------------------------------------------
std::string get_cwd() {
	std::string thisFile = GET_THIS_FILE_PATH();
	thisFile.erase(thisFile.begin() + thisFile.find_last_of('/'),
				   thisFile.end());
	return thisFile;
}

std::string get_file_path(const char *filename) {
	auto filepath = get_cwd();
	filepath.append("/");
	filepath.append(filename);
	return filepath;
}

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================

int main() {
	auto fn = get_file_path("testfile.txt");
	// const char *fn = "testfile.txt"; // file name
	const char *str = "This is a literal c-string.\n";

	// create/write the file
	puts("writing file");
	FILE *fw = fopen(fn.c_str(), "w");
	for (int i = 0; i < repeat; i++) {
		fputs(str, fw);
	}

	fclose(fw);
	puts("done.");

	// read the file
	puts("reading file");
	char buf[maxstring];
	FILE *fr = fopen(fn.c_str(), "r");
	while (fgets(buf, maxstring, fr)) {
		fputs(buf, stdout);
	}

	fclose(fr);
	remove(fn.c_str());

	puts("done.");

	return EXIT_SUCCESS;
}
