#include <cstdio>
#include <cmath>

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================

int main() {

	int i = -42;
	printf("abs(i): %d\n", std::abs(i));

	auto e = std::exp(1);
	printf("e: %lf\n", e);

	double x = log(e);
	printf("x: %lf\n", x);

	printf("pow(2,10): %lf\n", std::pow(2, 10));
	printf("sqrt(1024): %lf\n", std::sqrt(1024));

	printf("Program ended with exit code: %d\n", EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
