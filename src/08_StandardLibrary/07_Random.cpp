#include <cstdio>
#include <cstdlib>
#include <ctime>

#if defined(__cplusplus) && __cplusplus >= 201103L
#include <random> // Random number generators and distributions
#endif

int main() {

	/// =======================================================================
	/// @section Pseudo Random Number Generation
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection rand (old C random - not cryptographically secure
	/// -----------------------------------------------------------------------

	printf("time value: %ld\n", (long)time(nullptr));

	// seed the random number generation
	srand((unsigned)time(nullptr));
	rand(); // burn the first random number to remove bias and patterns

	printf("pseudo-random value(rand): %d\n", rand() % 1000);
	printf("pseudo-random value(rand): %d\n", rand() % 1000);
	printf("pseudo-random value(rand): %d\n", rand() % 1000);

	printf("RAND_MAX is %d\n", RAND_MAX);

	/// -----------------------------------------------------------------------
	/// @subsection Uniform Int Distribution (new C++ random - still not secure)
	/// -----------------------------------------------------------------------

	std::random_device
		rd; // Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
	std::uniform_int_distribution<> distrib(1, 1000);

	printf("pseudo-random value(mt19937): %d\n", distrib(gen));
	printf("pseudo-random value(mt19937): %d\n", distrib(gen));
	printf("pseudo-random value(mt19937): %d\n", distrib(gen));

	/// -----------------------------------------------------------------------
	/// @subsection Cryptographically Secure Pseudo Random Number Generator
	/// -----------------------------------------------------------------------
	/// @sa Advanced Encryption Standard (AES) with 128 or 256 bits keys

	return 0;
}
