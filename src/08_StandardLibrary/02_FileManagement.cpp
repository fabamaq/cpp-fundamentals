#include <cstdio>
#include <cstdlib>

#include <string>

/// ===========================================================================
/// @section Helper functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection returns a string of this file path thanks to the __FILE__ macro
/// ---------------------------------------------------------------------------
std::string get_file(const char *file) { return std::string(file); }
#define GET_THIS_FILE_PATH() get_file(__FILE__)

/// ---------------------------------------------------------------------------
/// @subsection returns the cwd by erasing "02_RAII.cpp" from this file path
/// ---------------------------------------------------------------------------
std::string get_cwd() {
	std::string thisFile = GET_THIS_FILE_PATH();
	thisFile.erase(thisFile.begin() + thisFile.find_last_of('/'),
				   thisFile.end());
	return thisFile;
}

std::string get_file_path(const char *filename) {
	auto filepath = get_cwd();
	filepath.append("/");
	filepath.append(filename);
	return filepath;
}

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================

int main() {
	auto fn = get_file_path("file1");
    FILE * fw = fopen(fn.c_str(), "w");
    fclose(fw);
    puts("done.");

    puts("Press any key to continue...");
	int c;
	c = getchar();

    puts("renaming file");
    auto fn2 = get_file_path("file2");
    rename(fn.c_str(), fn2.c_str());

	return EXIT_SUCCESS;
}
