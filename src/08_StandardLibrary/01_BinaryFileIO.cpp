#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>

constexpr size_t maxlen = 127;

struct s1 {
	uint8_t num;
	uint8_t len;
	char s[maxlen + 1];
};

/// ===========================================================================
/// @section Helper functions
/// ===========================================================================

/// ---------------------------------------------------------------------------
/// @subsection returns a string of this file path thanks to the __FILE__ macro
/// ---------------------------------------------------------------------------
std::string get_file(const char *file) { return std::string(file); }
#define GET_THIS_FILE_PATH() get_file(__FILE__)

/// ---------------------------------------------------------------------------
/// @subsection returns the cwd by erasing "02_RAII.cpp" from this file path
/// ---------------------------------------------------------------------------
std::string get_cwd() {
	std::string thisFile = GET_THIS_FILE_PATH();
	thisFile.erase(thisFile.begin() + thisFile.find_last_of('/'),
				   thisFile.end());
	return thisFile;
}

std::string get_file_path(const char *filename) {
	auto filepath = get_cwd();
	filepath.append("/");
	filepath.append(filename);
	return filepath;
}

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================

int main() {
	auto fn = get_file_path("test.file");
	const char *str = "This is a literal C-string.";

	// create/write the file
	puts("writing file");
	FILE *fw = fopen(fn.c_str(), "wb");

	static s1 buf1;
	for (int i = 0; i < 5; i++) {
		buf1.num = i;
		buf1.len = (uint8_t)strlen(str);
		if (buf1.len > maxlen)
			buf1.len = maxlen;
		strncpy(buf1.s, str, maxlen);
		buf1.s[buf1.len] = 0; // make sure it's terminated
		fwrite(&buf1, sizeof(s1), 1, fw);
	}

	fclose(fw);
	puts("done.");

	// read the file
	puts("reading file");
	FILE *fr = fopen(fn.c_str(), "rb");
	static s1 buf2;
	size_t rc;
	while ((rc = fread(&buf2, sizeof(s1), 1, fr))) {
		printf("a: %d, b: %d, s: %s\n", buf2.num, buf2.len, buf2.s);
	}

	fclose(fr);
	// remove(fn.c_str());

	puts("done.");

	return EXIT_SUCCESS;
}
