#include <cstdio>
#include <cstdlib>

/// ===========================================================================
/// @brief main - application entry point
/// ===========================================================================

int main() {

	/// =======================================================================
	/// @section Console I/O
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection Unformatted character I/O
	/// -----------------------------------------------------------------------

	const int bufsize = 256;
	static char buf[bufsize];
	fputs("prompt: ", stdout);
	fgets(buf, bufsize, stdin);
	puts("output:");
	fputs(buf, stdout);
	fflush(stdout);

	/// -----------------------------------------------------------------------
	/// @subsection Formatted character I/O
	/// -----------------------------------------------------------------------

	int i = 5;
	long int li = 1234567890L;
	const char *s = "This is a string.";
	printf("i is %d, li is %ld, s is %s\n", i, li, s);
	fprintf(stdout, "pointer is %p, sizeof is %zd\n", s, sizeof(s));

	printf("Program ended with exit code: %d\n", EXIT_SUCCESS);
	return EXIT_SUCCESS;
}
