// single-line comment
/*
	This is a multi-line comment.
	It is inherited from the C language.
	We can put our copyright notice here.
 */
// ============================================================================

/*
	Next, we have an #include statement
	An #include statement copies the contents of the given file
	into this current file at the preprocessing phase of the build process
*/
#include "StandardLibrary.hpp" // include all the standard library headers

// This is a declaration statement about the existence of the externIntVariable
extern int externIntVariable;

// This is a definition statement about the existence of the globalIntVariable
int globalIntVariable;

/*
	The difference between declaration and definition is:

	- Declarations introduce names and types without giving details,
	... such as where storage is located or how things are implemented.

	Examples:
		extern int x;					// object declaration
		class Widget;					// class declaration
		bool func(const Widget& x)		// function declaration
		enum class Color;				// scoped enum declaration

	- Definitions provide the storage locations or implementation details.

	Examples:
		int x;							// object definition
		class Widget {					// class definition
			...
		};
		bool func(const Widget& x) {	// function definiiton
			return w.size() < 0;
		}
		enum class Color {				// scoped enum declaration
			Yellow, Red, Blue
		};

	Notes:
		- Definitions are inheritedly declarations too
		- Prefer declarations over definitions if possible

*/

int main() {
	// a statement that defines a pointer '*' to a constant character to ...
	const char *message = "Hello World!"; // ... a string literal

	/// @note a string literal ends with an "invisible" '\0' null character
	/// ... that's how the application knows where is the end of the string

	// we pass 'message' as an argument to the 'puts' function
	puts(message);

	/// =======================================================================
	/// @section Sequence Statements
	/// =======================================================================
	const int strongestMagicNumber{7}; // initialization statement
	const int luckiestMagicNumber{3};  // !never forget to initialize a variable
	const int myFavouriteNumber = 21;  // this is also an initialization

	// these function call statements will execute in sequence
	printf("the most powerful number in magic is: %d\n", strongestMagicNumber);
	printf("the luckiest number in magic is: %d\n", luckiestMagicNumber);
	printf("my favourite number is: %d\n", myFavouriteNumber);

	printf("What is your favourite number?\n> ");
	int yourFavouriteNumber;
	scanf("%d", &yourFavouriteNumber);
	printf("your favourite number is: %d\n", yourFavouriteNumber);

	/// =======================================================================
	/// @section Conditional Statements
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection if statements
	/// -----------------------------------------------------------------------
	if (yourFavouriteNumber == myFavouriteNumber) {
		puts("We have the same favourite number");
	} else {
		puts("My favourite number is better than yours");
	}

	/// -----------------------------------------------------------------------
	/// @subsection switch statements
	/// -----------------------------------------------------------------------
	switch (yourFavouriteNumber) {
	case luckiestMagicNumber:
		puts("Your favourite number is the luckiest");
		break;
	case strongestMagicNumber:
		puts("Your favourite number is the most magical");
		break;
	default:
		puts("Your favourite number is just that, yours");
		break;
	}

	/// =======================================================================
	/// @section Iteration Statements
	/// =======================================================================

	/// -----------------------------------------------------------------------
	/// @subsection while statements
	/// -----------------------------------------------------------------------
	int counter = luckiestMagicNumber;
	puts("Next is a while loop statement");
	puts("it will run until the condition is no longer satisfied");
	while (counter > 0) {
		counter = counter - 1;
		printf("counter > 0? well, counter is %d\n", counter);
	};

	/// -----------------------------------------------------------------------
	/// @subsection do-while statements
	/// -----------------------------------------------------------------------
	puts("Next is a do-while loop statement");
	puts("It will run at least once and until the condition is false");
	do {
		counter = counter + 1;
		printf("counter < luckiestMagicNumber? well, counter is %d\n", counter);
	} while (counter < luckiestMagicNumber);

	/// -----------------------------------------------------------------------
	/// @subsection for statements
	/// -----------------------------------------------------------------------
	puts("Next is a for loop");
	puts("it will run until the condition is no longer satisfied");
	// for (initial_state; condition; update)
	for (int i = 0; i < luckiestMagicNumber; i = i + 1) {
		printf("i < luckiestMagicNumber? well, i is %d. After this it will be "
			   "%d\n",
			   i, i + 1);
	}

	puts("Press any key to continue...");
	int c;
	c = getchar();
	c = getchar();

	return 0;
}
